The main application 'Keyboard Manager' source 
code is in the 'Keyboardmanager' directory.
After building the apk file, the application 
needs to be installed as a system application 
in order for it to function.
To do so, it needs to be moved under the '/system/app/' directory.

The application 'KmLauncher' packages 'Keyboard Manager',
then takes care of setting the filesystem, 
and installing 'Keyboard Manager' as a system application.
It also shows up in the app drawer to allow the user to access 'Keyboard Manager' settings screen.

To package 'Keyboard Manager' with 'KmLauncher', 
build 'Keyboard Manager' apk file, 
and copy it under 'KmLauncher' assets directory (./KmLauncher/assets/app/).
Modify 'KmLauncher/src/com/ne0fhykLabs/android/utility/kmLauncher/Launcher.java',
and update the 'apkName' field to reflect the name of the 'Keyboard Manager' apk file under './KmLauncher/assets/app/'.
