/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmplus.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.util.Xml;
import com.ne0fhykLabs.android.utility.kmplus.R;
import com.ne0fhykLabs.android.utility.kmplus.utils.Constants;
import com.ne0fhykLabs.android.utility.kmplus.utils.KeyboardPreferences;
import com.ne0fhykLabs.android.utility.kmplus.utils.Utils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.*;
import java.util.*;

import static com.ne0fhykLabs.android.utility.kmplus.utils.Constants.*;

public class KMPlusSettings extends PreferenceActivity {

    private static final String TAG = KMPlusSettings.class.getName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preference);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupPreferenceScreen();
    }

    private void setupPreferenceScreen() {
        final Context context = getApplicationContext();
        final SharedPreferences settings = getPreferenceManager().getSharedPreferences();

        // Figure out if the accessibility service is enabled
        final boolean isServiceEnabled = Utils
                .isAccessibilityServiceEnabled(context);

        Preference startPref = findPreference(Constants.START_KMP_PREF);
        KeyboardPreferences kbPref = (KeyboardPreferences) findPreference(Constants
                .GLOBAL_KEYBOARD_PREF_KEY);
        PreferenceScreen appsPrefScreen = (PreferenceScreen) findPreference(Constants
                .APPS_KEYBOARD_PREF_SCREEN_KEY);

        if (isServiceEnabled) {
            startPref.setTitle(R.string.title_start_pref_disable);
            startPref.setSummary(R.string.summary_start_pref_disable);
            startPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Utils.accessAccessibilityService(KMPlusSettings.this, false);
                    return true;
                }
            });

            kbPref.setEnabled(true);

            appsPrefScreen.setEnabled(true);
            updateAppScreen();
        }
        else {
            startPref.setTitle(R.string.title_start_pref_enable);
            startPref.setSummary(R.string.summary_start_pref_enable);
            startPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Utils.accessAccessibilityService(KMPlusSettings.this, true);
                    return true;
                }
            });

            kbPref.setEnabled(false);
            appsPrefScreen.setEnabled(false);
        }

        // Utility preferences
        String defaultBackupFilepath =
                new File(getExternalFilesDir(null), DEFAULT_BACKUP_FILENAME).toString();
        String backupFilepath = settings.getString(BACKUP_FILEPATH_PREF, defaultBackupFilepath);

        EditTextPreference backupPref = (EditTextPreference) findPreference(BACKUP_PREF);
        backupPref.setText(backupFilepath);
        backupPref.setTitle("Backup preferences");
        backupPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Context context = getApplicationContext();

                // Store the application preferences to the path in newValue
                String backupPath = (String) newValue;

                settings.edit().putString(BACKUP_FILEPATH_PREF, backupPath).commit();

                File backupFile = new File(backupPath);
                File parentDir = backupFile.getParentFile();
                if (!parentDir.mkdirs() && !parentDir.isDirectory()) {
                    Utils.toastMessage(context, "Unable to create backup directory.");
                    return false;
                }

                // Copy the shared preferences file to the backup location
                boolean result = exportPreferences(backupFile);
                if (result)
                    Utils.toastMessage(context, "Backup complete!");
                else
                    Utils.toastMessage(context, "Backup failed!");

                return result;
            }
        });

        EditTextPreference restorePref = (EditTextPreference) findPreference(RESTORE_PREF);
        restorePref.setText(backupFilepath);
        restorePref.setTitle("Restore preferences");
        restorePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Context context = getApplicationContext();

                String restorePath = (String) newValue;

                File restoreFile = new File(restorePath);
                if (!restoreFile.exists()) {
                    Utils.toastMessage(context, "Unable to locate preferences backup.");
                    return false;
                }

                boolean result = importPreferences(restoreFile);
                if (result) {
                    settings.edit().putString(BACKUP_FILEPATH_PREF, restorePath).commit();

                    // Update the preference screen
                    setupPreferenceScreen();
                    Utils.toastMessage(context, "Restore complete!");
                }
                else
                    Utils.toastMessage(context, "Restore failed!");

                return result;
            }
        });

    }

    public void updateAppScreen() {
        final Context context = getApplicationContext();
        PreferenceScreen appsPrefScreen = (PreferenceScreen) findPreference(Constants
                .APPS_KEYBOARD_PREF_SCREEN_KEY);

        Preference addAppsPref = appsPrefScreen.findPreference(Constants.ADD_APPS_PREF_KEY);
        appsPrefScreen.removeAll();

        addAppsPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                Utils.selectAppDialog(KMPlusSettings.this);
                return true;
            }
        });

        appsPrefScreen.addPreference(addAppsPref);

        // Load the list of enabled apps, and add it to the preference
        // screen
        final Map<String, String> appsInfo = Utils.getEnabledAppList(context);

        for (Map.Entry<String, String> entry : appsInfo.entrySet()) {
            KeyboardPreferences appPreference = new KeyboardPreferences(this, null);
            String appName = entry.getKey();
            appPreference.setTitle(appName);
            appPreference.setSummary("Keyboard preferences for " + appName);
            appPreference.setKey(Constants.APP_KEYBOARD_PREF_KEY + "." + entry.getValue());

            appsPrefScreen.addPreference(appPreference);
        }
    }

    private void writePreferenceToXml(final XmlSerializer initializedSerializer,
                                      final String namespace, String prefKey, Object prefValue)
            throws IOException {

        final String nameAttr = "name";
        final String valueAttr = "value";

        if (prefValue instanceof Boolean) {
            Boolean value = (Boolean) prefValue;
            initializedSerializer.startTag(namespace, "boolean");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "boolean");
        }
        else if (prefValue instanceof Float) {
            Float value = (Float) prefValue;
            initializedSerializer.startTag(namespace, "float");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "float");
        }
        else if (prefValue instanceof Integer) {
            Integer value = (Integer) prefValue;
            initializedSerializer.startTag(namespace, "int");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "int");
        }
        else if (prefValue instanceof Long) {
            Long value = (Long) prefValue;
            initializedSerializer.startTag(namespace, "long");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "long");
        }
        else if (prefValue instanceof String) {
            String value = (String) prefValue;
            initializedSerializer.startTag(namespace, "string");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.text(value);
            initializedSerializer.endTag(namespace, "string");
        }
        else if (prefValue instanceof Set) {
            Set<String> value = (Set<String>) prefValue;
            initializedSerializer.startTag(namespace, "set");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);

            for (String setEntry : value) {
                initializedSerializer.startTag(namespace, "string");
                initializedSerializer.text(setEntry);
                initializedSerializer.endTag(namespace, "string");
            }

            initializedSerializer.endTag(namespace, "set");
        }
    }

    private boolean exportPreferences(File exportDestination) {
        boolean result = true;

        final String namespace = null;

        final XmlSerializer serializer = Xml.newSerializer();
        final SharedPreferences settings = getPreferenceManager().getSharedPreferences();
        final Map<String, ?> settingsContent = settings.getAll();

        try {
            FileWriter out = new FileWriter(exportDestination);
            try {
                serializer.setOutput(out);
                serializer.startDocument("UTF-8", true);
                serializer.startTag(namespace, "map");

                for (Map.Entry<String, ?> prefEntry : settingsContent.entrySet()) {
                    String prefKey = prefEntry.getKey();
                    Object prefValue = prefEntry.getValue();

                    writePreferenceToXml(serializer, namespace, prefKey, prefValue);
                }

                serializer.endTag(namespace, "map");
                serializer.endDocument();

            } finally {
                out.close();
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        }

        return result;
    }

    private boolean importPreferences(File importFile) {
        boolean result = true;
        FileReader in = null;

        final String namespace = null;
        final String nameAttr = "name";
        final String valueAttr = "value";

        // Initialize the variables to store the data from the xml file.
        final class NameValuesPair {
            final String name;
            final String[] valueList;

            NameValuesPair(String name, String... value) {
                this.name = name;
                this.valueList = value;
            }

            @Override
            public String toString() {
                return "[" + name + " => " + Arrays.toString(valueList) + "]";
            }
        }

        final Map<String, List<NameValuesPair>> importContents = new HashMap<String,
                List<NameValuesPair>>();

        final XmlPullParser parser = Xml.newPullParser();
        try {
            in = new FileReader(importFile);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in);
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, namespace, "map");

            for (int eventType = parser.next(); eventType != XmlPullParser.END_DOCUMENT;
                 eventType = parser
                    .next()) {
                if (eventType == XmlPullParser.START_TAG) {
                    String tagName = parser.getName();
                    if (tagName == null)
                        continue;

                    String name = parser.getAttributeValue(namespace, nameAttr);
                    NameValuesPair pair = null;

                    if (tagName.equals("set")) {
                        List<String> valueList = new ArrayList<String>();

                        for (eventType = parser.next(); eventType != XmlPullParser.END_DOCUMENT;
                             eventType = parser
                                .next()) {
                            if (eventType == XmlPullParser.START_TAG) {
                                valueList.add(parser.nextText());
                            }
                            else if (eventType == XmlPullParser.END_TAG) {
                                String childTagName = parser.getName();
                                if (childTagName.equals("set"))
                                    break;
                            }
                        }

                        pair = new NameValuesPair(name, valueList.toArray(new String[0]));
                    }
                    else {
                        String value = tagName.equals("string")
                                ? parser.nextText()
                                : parser.getAttributeValue(namespace,
                                valueAttr);
                        pair = new NameValuesPair(name, value);
                    }

                    if (pair != null) {
                        List<NameValuesPair> tagContents = importContents.get(tagName);
                        if (tagContents == null)
                            tagContents = new ArrayList<NameValuesPair>();

                        tagContents.add(pair);
                        importContents.put(tagName, tagContents);
                    }
                }
            }

        } catch (XmlPullParserException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Unable to locate restore file.", e);
            Utils.toastMessage(getApplicationContext(), "Unable to locate restore file.");
            result = false;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        if (importContents.size() > 0) {
            final SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences()
                    .edit();

            // Clear the previous preferences
            editor.clear();

            for (Map.Entry<String, List<NameValuesPair>> prefEntry : importContents.entrySet()) {
                String tagName = prefEntry.getKey();
                List<NameValuesPair> prefPairList = prefEntry.getValue();

                for (NameValuesPair prefPair : prefPairList) {
                    String prefKey = prefPair.name;
                    String[] prefValueList = prefPair.valueList;

                    if (tagName == null)
                        continue;
                    else if (tagName.equals("boolean")) {
                        for (String prefValue : prefValueList)
                            editor.putBoolean(prefKey, Boolean.parseBoolean(prefValue));
                    }
                    else if (tagName.equals("float")) {
                        for (String prefValue : prefValueList)
                            editor.putFloat(prefKey, Float.parseFloat(prefValue));
                    }
                    else if (tagName.equals("int")) {
                        for (String prefValue : prefValueList)
                            editor.putInt(prefKey, Integer.parseInt(prefValue));
                    }
                    else if (tagName.equals("long")) {
                        for (String prefValue : prefValueList)
                            editor.putLong(prefKey, Long.parseLong(prefValue));
                    }
                    else if (tagName.equals("string")) {
                        for (String prefValue : prefValueList)
                            editor.putString(prefKey, prefValue);
                    }
                    else if (tagName.equals("set")) {
                        Set<String> prefValueSet = new HashSet<String>(prefValueList.length);
                        for (String prefValue : prefValueList)
                            prefValueSet.add(prefValue);

                        editor.putStringSet(prefKey, prefValueSet);
                    }
                }
            }

            // Commit the changes
            result = editor.commit();
        }

        return result;
    }
}
