/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmplus.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;

import com.ne0fhykLabs.android.utility.kmplus.utils.Constants;
import com.ne0fhykLabs.android.utility.kmplus.utils.Utils;
import com.ne0fhykLabs.android.utility.kmplus.utils.Utils.ConfigurationType;
import com.ne0fhykLabs.android.utility.kmplus.utils.Utils.DockState;

public class AppSwitchDetector extends AccessibilityService {

    private static final String TAG = AppSwitchDetector.class.getName();

    private final static long IMP_RECEIPT_TIME_INTERVAL = 500l; // 0.5 seconds
    private final static AtomicLong sIMPLaunchTime = new AtomicLong(0l);
    private final static AtomicBoolean sResetIme = new AtomicBoolean(false);

    private final static Map<String, InputMethodInfo> sImiCache = new HashMap<String, InputMethodInfo>();

    private final AtomicBoolean mConfigurationChanged = new AtomicBoolean(false);
    private String sLastValidPackageName;

    protected static final IntentFilter sDockFilter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
    protected final BroadcastReceiver mDockEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            String action = intent.getAction();
            if ( Intent.ACTION_DOCK_EVENT.equals(action) ) {
                Log.d(TAG, "Docking event received.");
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        loadImiCache();
        mConfigurationChanged.set(false);

        Log.d(TAG, "KMPlus Service created.");
        registerReceiver(mDockEventReceiver, sDockFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        flushImiCache();
        mConfigurationChanged.set(false);

        Log.d(TAG, "KMPlus Service destroyed.");
        unregisterReceiver(mDockEventReceiver);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * android.accessibilityservice.AccessibilityService#onAccessibilityEvent(android.view.accessibility
     * .AccessibilityEvent)
     */
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        final String className = event.getClassName().toString();
        final String packageName = event.getPackageName().toString();
        int eventType = event.getEventType();
        Log.d(TAG, "Package name: " + packageName);

        if ( eventType == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED &&
             !mConfigurationChanged.get() ) {
            return;
        }

        boolean isWithinIMPLaunchWindow = (System.currentTimeMillis() - sIMPLaunchTime.get()) < IMP_RECEIPT_TIME_INTERVAL;
        if ( (packageName.equals(Constants.PACKAGE_NAME_ALERT_DIALOG) || packageName
                .startsWith(Constants.PACKAGE_NAME_ANDROID_SYSTEM_UI_PREFIX)) &&
             className.equals(Constants.CLASS_NAME_ALERT_DIALOG) && isWithinIMPLaunchWindow ) {
            handleInputMethodPicker(event);
        }
        else if ( packageName.startsWith(Constants.PACKAGE_NAME_ANDROID_SYSTEM_UI_PREFIX) ||
                  className.startsWith(Constants.CLASS_NAME_ANDROID_INTERNAL_PREFIX) ) {
            // Event to avoid reacting too
            Log.d(TAG, "Ignoring event with class name " + className);
        }
        else {
            synchronized (sDockFilter) {
                // Check if we're dealing with the same package as before
                if ( packageName.equals(sLastValidPackageName) && !mConfigurationChanged.get() )
                    return;


                if ( mConfigurationChanged.compareAndSet(true, false) || sResetIme.get() ) {
                    launchInputMethodPicker(packageName);
                }
                else if ( getSelectedPackageNames().contains(packageName) ) {
                    sResetIme.set(true);
                    launchInputMethodPicker(packageName);
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "Configuration changed called.");
        mConfigurationChanged.set(true);
    }

    private Set<String> getSelectedPackageNames() {
        // Retrieve the list of packages with specific configuration
        Set<String> selectedPackageNames = new HashSet<String>(PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext())
                .getStringSet(Constants.APPS_SET_KEY, Collections.<String> emptySet()));

        // Add our own package name.
        selectedPackageNames.add(Constants.APP_PACKAGE_NAME);

        return selectedPackageNames;
    }

    private void flushImiCache() {
        sImiCache.clear();
    }

    private void loadImiCache() {
        flushImiCache();

        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        List<InputMethodInfo> imis = imm.getEnabledInputMethodList();
        for ( InputMethodInfo imi : imis ) {
            sImiCache.put(imi.getId(), imi);
        }
    }

    private InputMethodInfo retrieveFromCache(String imiId) {
        InputMethodInfo cachedValue = sImiCache.get(imiId);
        if ( cachedValue == null ) {
            // Reload the cache in case it's stale.
            loadImiCache();
            cachedValue = sImiCache.get(imiId);
        }

        return cachedValue;
    }

    private String getConfigurationKeyboardId() {
        // Retrieve the current configuration
        List<ConfigurationType> configTypeList = new ArrayList<ConfigurationType>();

        // Docking state has priority over other configurations
        switch (getDockState()) {
            case CAR:
                configTypeList.add(ConfigurationType.CAR_DOCK);
                break;

            case DESK:
                configTypeList.add(ConfigurationType.DESK_DOCK);
                break;

            case UNDOCKED:
            default:
                // nothing to do
                break;
        }

        final Configuration systemConfig = getResources().getConfiguration();
        if ( systemConfig.keyboard != Configuration.KEYBOARD_NOKEYS &&
             systemConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO ) {
            configTypeList.add(ConfigurationType.HARDWARE);
        }
        else {
            switch (systemConfig.orientation) {
                case Configuration.ORIENTATION_LANDSCAPE:
                    configTypeList.add(ConfigurationType.LANDSCAPE);
                    break;

                case Configuration.ORIENTATION_PORTRAIT:
                    configTypeList.add(ConfigurationType.PORTRAIT);
                    break;
            }
        }

        if ( configTypeList.isEmpty() )
            return Constants.DEFAULT_KEYBOARD_ID;

        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        // App preferences
        String currentPackageName = sLastValidPackageName;
        if ( currentPackageName != null ) {

            for ( ConfigurationType config : configTypeList ) {
                String prefKey = Utils.getAppKeyboardIdKey(currentPackageName, config);
                String keyboardLabel = settings.getString(prefKey, null);
                if ( keyboardLabel != null )
                    return keyboardLabel;
            }
        }

        // Global preferences
        for ( ConfigurationType config : configTypeList ) {
            String prefKey = Utils.getGlobalKeyboardIdKey(config);
            String keyboardLabel = settings.getString(prefKey, null);
            if ( keyboardLabel != null )
                return keyboardLabel;
        }

        return Constants.DEFAULT_KEYBOARD_ID;
    }

    private void handleInputMethodPicker(final AccessibilityEvent event) {

        boolean dismissIMP = true;

        final AccessibilityNodeInfo nodeInfo = event.getSource();
        if ( nodeInfo != null ) {

            String configKeyboardId = getConfigurationKeyboardId();
            if ( !configKeyboardId.equals(Constants.DEFAULT_KEYBOARD_ID) ) {

                // Get the label from the keyboard id
                InputMethodInfo configImi = retrieveFromCache(configKeyboardId);
                if ( configImi != null ) {
                    CharSequence configKeyboardLabel = configImi.loadLabel(getPackageManager());

                    if ( configKeyboardLabel != null ) {
                        List<AccessibilityNodeInfo> keyboardList = nodeInfo
                                .findAccessibilityNodeInfosByText(configKeyboardLabel.toString());

                        if ( keyboardList.size() > 0 ) {
                            AccessibilityNodeInfo keyboardNodeInfo = keyboardList.get(0);
                            AccessibilityNodeInfo keyboardContainer = keyboardNodeInfo.getParent();

                            if ( keyboardContainer
                                    .performAction(AccessibilityNodeInfo.ACTION_CLICK) ) {
                                dismissIMP = false;

                                if ( !getSelectedPackageNames().contains(sLastValidPackageName) )
                                    sResetIme.set(false);
                            }

                            keyboardNodeInfo.recycle();
                            keyboardContainer.recycle();
                        }
                    }
                }
            }

            nodeInfo.recycle();
        }

        if ( dismissIMP ) {
            Log.d(TAG, "Performing global back action.");
            performGlobalAction(GLOBAL_ACTION_BACK);
        }
    }

    private void launchInputMethodPicker(String currentPackageName) {
        long currentTime = System.currentTimeMillis();
        long lastImpLaunchTime = sIMPLaunchTime.get();
        if ( (currentTime - lastImpLaunchTime) < IMP_RECEIPT_TIME_INTERVAL )
            return;

        if ( sIMPLaunchTime.compareAndSet(lastImpLaunchTime, currentTime) ) {
            sLastValidPackageName = currentPackageName;
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showInputMethodPicker();
        }
    }

    private DockState getDockState() {
        Intent dockStatus = registerReceiver(null, sDockFilter);
        return Utils.getDockStateHelper(dockStatus);
    }

    @SuppressWarnings("unused")
    @SuppressLint("NewApi")
    private void traverseNodeInfo(final AccessibilityNodeInfo nodeInfo) {
        int childCount = nodeInfo.getChildCount();
        CharSequence nodeClassName = nodeInfo.getClassName();
        CharSequence packageName = nodeInfo.getPackageName();
        CharSequence description = nodeInfo.getContentDescription();
        CharSequence nodeText = nodeInfo.getText();
        List<AccessibilityNodeInfo> childByViewIdList = nodeInfo
                .findAccessibilityNodeInfosByViewId("com.android.internal:id/alertTitle");
        AccessibilityNodeInfo labelFor = nodeInfo.getLabelFor();
        AccessibilityNodeInfo labelBy = nodeInfo.getLabeledBy();
        AccessibilityNodeInfo parent = nodeInfo.getParent();
        String viewIdResName = nodeInfo.getViewIdResourceName();
        int windowId = nodeInfo.getWindowId();

        for ( int i = 0; i < childCount; i++ ) {
            final AccessibilityNodeInfo childInfo = nodeInfo.getChild(i);
            if ( childInfo == null )
                continue;
            traverseNodeInfo(childInfo);
            childInfo.recycle();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.accessibilityservice.AccessibilityService#onInterrupt()
     */
    @Override
    public void onInterrupt() {
        // TODO Auto-generated method stub

    }
}
