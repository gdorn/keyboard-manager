/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmplus.utils;


public class Constants {

    public static final String APP_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.kmplus";

    public static final String CLASS_NAME_ALERT_DIALOG = "android.app.AlertDialog";
    public static final String PACKAGE_NAME_ALERT_DIALOG = "android";

    public static final String PACKAGE_NAME_ANDROID_SYSTEM_UI_PREFIX = "com.android.systemui";
    public static final String CLASS_NAME_ANDROID_INTERNAL_PREFIX = "com.android.internal";

    public static final String DEFAULT_KEYBOARD_LABEL = "Default";
    public static final String DEFAULT_KEYBOARD_ID = "default_id";

    public static final String KEYBOARD_PREF_PREFIX = "keyboardLabel";
    public static final String GLOBAL_KEYBOARD_PREF_KEY = "global_pref";
    public static final String APP_KEYBOARD_PREF_KEY = "app_pref";
    public static final String APPS_KEYBOARD_PREF_SCREEN_KEY = "appskeyboard_pref";
    public static final String ADD_APPS_PREF_KEY = "add_apps_pref";
    public static final String APP_PACKAGE_NAME_KEY = "app_package_name_key";
    public static final String APPS_SET_KEY = "apps_set_key";

    public static final String START_KMP_PREF = "start-pref";
    public static final String BACKUP_PREF = "backup_pref";
    public static final String RESTORE_PREF = "restore_pref";

    public static final String BACKUP_FILEPATH_PREF = "backup_filepath";
    public static final String DEFAULT_BACKUP_FILENAME = "kmplus_preferences_backup.xml";

}
