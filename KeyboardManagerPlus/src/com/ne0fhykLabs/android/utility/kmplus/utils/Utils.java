/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmplus.utils;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.ne0fhykLabs.android.utility.kmplus.R;
import com.ne0fhykLabs.android.utility.kmplus.activity.KMPlusSettings;

public class Utils {

    private static final String TAG = Utils.class.getName();

    public enum ConfigurationType {
        PORTRAIT,
        LANDSCAPE,
        HARDWARE,
        BLUETOOTH,
        DESK_DOCK,
        CAR_DOCK
    }

    public enum DockState {
        CAR,
        DESK,
        UNDOCKED
    }

    public static void accessAccessibilityService(final Activity activity,
            final boolean shouldEnable) {
        final int dialogTitleId = shouldEnable ? R.string.title_enable_kmp_dialog
                                              : R.string.title_disable_kmp_dialog;
        final int dialogMsgId = shouldEnable ? R.string.text_enable_kmp_dialog
                                            : R.string.text_disable_kmp_dialog;

        final int positiveTextId = shouldEnable ? R.string.text_enable_kmp_dialog_positive_button
                                               : R.string.text_disable_kmp_dialog_positive_button;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(dialogTitleId)
                .setMessage(dialogMsgId)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton(positiveTextId, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent settingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        activity.startActivity(settingsIntent);
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static DockState getDockStateHelper(Intent dockIntent) {
        if ( dockIntent == null )
            return DockState.UNDOCKED;

        int dockState = dockIntent.getIntExtra(Intent.EXTRA_DOCK_STATE,
                                               Intent.EXTRA_DOCK_STATE_UNDOCKED);

        switch (dockState) {
            case Intent.EXTRA_DOCK_STATE_CAR:
                return DockState.CAR;

            case Intent.EXTRA_DOCK_STATE_DESK:
            case Intent.EXTRA_DOCK_STATE_LE_DESK:
            case Intent.EXTRA_DOCK_STATE_HE_DESK:
                return DockState.DESK;

            default:
            case Intent.EXTRA_DOCK_STATE_UNDOCKED:
                return DockState.UNDOCKED;

        }
    }

    public static String getGlobalKeyboardIdKey(ConfigurationType configuration) {
        return Constants.KEYBOARD_PREF_PREFIX + "." + Constants.GLOBAL_KEYBOARD_PREF_KEY + "."
               + configuration.name();
    }

    public static String getAppKeyboardIdKey(String packageName,
            ConfigurationType configuration) {
        return Constants.KEYBOARD_PREF_PREFIX + "." + Constants.APP_KEYBOARD_PREF_KEY + "."
               + packageName + "." + configuration.name();
    }

    public static Pair<CharSequence[], CharSequence[]> getKeyboardsInfo(Context context) {
        // Retrieve list of input method
        InputMethodManager imeManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        List<InputMethodInfo> imes = imeManager.getEnabledInputMethodList();
        int imesSize = imes.size();

        // Store input method labels for display in view menu
        CharSequence[] keyboardLabels = new CharSequence[imesSize + 1];
        CharSequence[] keyboardIds = new CharSequence[imesSize + 1];

        keyboardLabels[0] = Constants.DEFAULT_KEYBOARD_LABEL;
        keyboardIds[0] = Constants.DEFAULT_KEYBOARD_ID;

        PackageManager pm = context.getPackageManager();
        for ( int i = 1; i < imesSize + 1; i++ ) {
            InputMethodInfo imi = imes.get(i - 1);
            keyboardLabels[i] = imi.loadLabel(pm);
            keyboardIds[i] = imi.getId();
        }

        return new Pair<CharSequence[], CharSequence[]>(keyboardLabels, keyboardIds);
    }

    public static boolean isAccessibilityServiceEnabled(final Context context) {
        final String serviceLabel = context.getString(R.string.accessibility_app_name);
        final AccessibilityManager am =
                (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        PackageManager pm = context.getPackageManager();

        List<AccessibilityServiceInfo> enabledServiceList = am
                .getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_GENERIC);

        for ( AccessibilityServiceInfo info : enabledServiceList ) {
            ResolveInfo resolveInfo = info.getResolveInfo();
            String infoName = resolveInfo.loadLabel(pm).toString();
            if ( serviceLabel.equals(infoName) ) {
                return true;
            }
        }

        return false;
    }

    public static void toastMessage(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void toastMessage(Context context, int msgId) {
        Toast.makeText(context, msgId, Toast.LENGTH_LONG).show();
    }

    public static Map<String, String> getAllApps(Context context) {
        final PackageManager manager = context.getPackageManager();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null)
                .addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> apps = manager.queryIntentActivities(mainIntent, 0);

        if ( apps == null ) {
            Log.e(TAG, "Unable to retrieve device applications.");
            return null;
        }

        Map<String, String> appsInfo = new TreeMap<String, String>();
        for ( ResolveInfo appInfo : apps ) {
            String appTitle = getAppTitle(context, appInfo.activityInfo.packageName);
            if ( appTitle == null )
                continue;

            appsInfo.put(appTitle, appInfo.activityInfo.packageName);
        }

        return appsInfo;
    }

    public static String getAppTitle(Context context, String packageName) {
        final PackageManager manager = context.getPackageManager();

        try {
            ApplicationInfo info = manager.getApplicationInfo(packageName, 0);
            return info.loadLabel(manager).toString();
        } catch (NameNotFoundException e) {
            Log.w(TAG, "Unable to find label for package name " + packageName);
            return null;
        }
    }

    public static void selectAppDialog(final Activity activity) {
        final Context context = activity.getApplicationContext();

        final Map<String, String> allAppsInfo = getAllApps(context);
        final Map<String, String> savedAppsInfo = getEnabledAppList(context);
        for ( String appTitle : savedAppsInfo.keySet() ) {
            if ( !allAppsInfo.containsKey(appTitle) )
                savedAppsInfo.remove(appTitle);
        }

        updateEnabledAppList(context, new HashSet<String>(savedAppsInfo.values()));

        final String[] dialogItems = allAppsInfo.keySet().toArray(new String[0]);
        final boolean[] checkedDialogItems = new boolean[dialogItems.length];
        for ( int i = 0; i < dialogItems.length; i++ ) {
            if ( savedAppsInfo.containsKey(dialogItems[i]) )
                checkedDialogItems[i] = true;
            else
                checkedDialogItems[i] = false;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Select app(s)")
                .setMultiChoiceItems(dialogItems, checkedDialogItems,
                                     new DialogInterface.OnMultiChoiceClickListener() {

                                         @Override
                                         public void onClick(DialogInterface dialog, int which,
                                                 boolean isChecked) {

                                             String item = dialogItems[which];

                                             if ( isChecked ) {
                                                 String itemPackageName = allAppsInfo.get(item);
                                                 savedAppsInfo.put(item, itemPackageName);
                                             }
                                             else {
                                                 savedAppsInfo.remove(item);
                                             }
                                         }
                                     })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Done", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Store the list of checked apps to the shared
                        // preferences
                        updateEnabledAppList(context, new HashSet<String>(savedAppsInfo.values()));

                        if ( activity instanceof KMPlusSettings )
                            ((KMPlusSettings) activity).updateAppScreen();
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static Map<String, String> getEnabledAppList(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        Set<String> cmpSet = settings.getStringSet(Constants.APPS_SET_KEY,
                                                   Collections.<String> emptySet());

        final Map<String, String> appsInfo = new TreeMap<String, String>();
        for ( String appPackageName : cmpSet ) {

            String appTitle = getAppTitle(context, appPackageName);
            if ( appTitle == null )
                continue;

            appsInfo.put(appTitle, appPackageName);
        }

        // Update the saved preference with the valid entries.
        updateEnabledAppList(context, new HashSet<String>(appsInfo.values()));

        return appsInfo;
    }

    public static void updateEnabledAppList(Context context, Set<String> packageNameSet) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putStringSet(Constants.APPS_SET_KEY, packageNameSet).commit();
    }
}
