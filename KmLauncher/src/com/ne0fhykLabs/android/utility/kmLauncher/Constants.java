package com.ne0fhykLabs.android.utility.kmLauncher;

/**
 * Contains application related constants.
 * @author fhuya
 * @since 2.10
 */
public class Constants
{
    public static final String SYSTEM_APP_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.km";
    public static final String KM_CLASS_NAME = "com.ne0fhykLabs.android.utility.km.activity.KeyboardManager";

    public static final String APK_NAME = "Keyboardmanager.2.11.apk";
    public static final String BUSYBOX_FILENAME = "busybox";

	public static final String PREF_KM_IS_INSTALLED = "km_is_installed";

	/**
	 * Android system apps path before Kitkat (version < 19).
	 * @since 2.10
	 */
	public static final String SYSTEM_APP_PATH_BEFORE_KITKAT = "/system/app/";

	/**
	 * Android system apps path after Kitkat (version >= 19).
	 * @since 2.10
	 */
	public static final String SYSTEM_APP_PATH_AFTER_KITKAT = "/system/priv-app/";

	/**
	 * Path to the directory where Android regular applications are stored.
	 * @since 2.10
	 */
	public static final String DATA_APP_PATH = "/data/app/";

	/**
	 * Path to the proc mounts file on Android.
	 * @since 2.10
	 */
	public static final String PROC_MOUNTS_PATH = "/proc/mounts";

	/**
	 * Path to the system directory on Android.
	 * @since 2.10
	 */
	public static final String SYSTEM_PATH = "/system";

    /**
     * Private constructor to prevent instantiation of the class.
     * @since 2.11
     */
    private Constants(){}

}
