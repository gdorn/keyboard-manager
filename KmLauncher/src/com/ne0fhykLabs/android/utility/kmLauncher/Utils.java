/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmLauncher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ne0fhykLabs.android.utility.kmLauncher.Constants.APK_NAME;

/**
 * Contains utilities classes, and methods for the application.
 * @author fhuya
 * @since 2.11
 */
public class Utils
{
    private static final String TAG = Utils.class.getName();

    public enum LauncherStatus {
        SUCCESS("operation completed"),
        FAILED_NO_ROOT_ACCESS("operation failed: no root access"),
        FAILED_GENERIC_ERROR("operation failed"),
        CANCELLED("operation cancelled");

        String msg;

        private LauncherStatus(String msg) {
            this.msg = msg;
        }

    }

    /**
     * Check if the latest KeyboardManager application is installed.
     *
     * @return boolean true if it's, false otherwise
     */
    public static boolean checkForApk() {
        Log.i(TAG, "Checking for install binary");

        try {
            final File apkFile = new File(getSystemAppPath().concat(APK_NAME));
            return apkFile.exists();
        } catch (final Exception e) {
            Log.w(TAG, e);
            return false;
        }
    }

    /**
     * Check the device for installed, and leftover files related to this application.
     * @return list of file related to the application
     */
    public static List<File> checkForOtherVersions() {
        final ArrayList<File> otherVersions = new ArrayList<File>();
        final List<String> appStorePaths = getKMDirs();

        for ( final String path : appStorePaths ) {

            final File appStore = new File(path);
            try {
                if ( !appStore.exists() )
                    continue;

                final File[] dirFiles = appStore.listFiles();
                if ( dirFiles == null )
                    continue;

                for ( final File dirFile : dirFiles ) {
                    final String filename = dirFile.getName();
                    if ( filename.startsWith("KeyboardManager")
                         || filename.startsWith("com.android.bg.keyboardmanager")
                         || filename.startsWith("Keyboardmanager")
                         || filename.startsWith("com.ne0fhykLabs.android.utility.km.") )
                        otherVersions.add(dirFile);
                }
            } catch (final Exception e) {
                Log.w(TAG, e);
            }
        }
        return otherVersions;
    }

    public static boolean checkForSystemApp(final PackageManager pm) {
        try {
            pm.getApplicationInfo(Constants.SYSTEM_APP_PACKAGE_NAME, 0);
            return true;
        } catch (final NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Returns the path to the system app directory.
     * Before Android Kitkat (version 19), that path was <code>/system/app</code>.
     * After Kitkat, it changed to <code>/system/priv-app</code>.
     * @return path to the system apps.
     * @since 2.10
     */
    public static String getSystemAppPath(){
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT)
            return Constants.SYSTEM_APP_PATH_BEFORE_KITKAT;
        else
            return Constants.SYSTEM_APP_PATH_AFTER_KITKAT;
    }

    /**
     * Returns a list of all possible directories where the KM app(s) could be installed.
     * Includes legacy installation directories.
     * @return list of KM app(s) directories.
     * @since 2.10
     */
    public static List<String> getKMDirs(){
        final List<String> kmDirs = new ArrayList<String>();
        kmDirs.add(Constants.DATA_APP_PATH);
        kmDirs.add(Constants.SYSTEM_APP_PATH_BEFORE_KITKAT);

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
            kmDirs.add(Constants.SYSTEM_APP_PATH_AFTER_KITKAT);

        return kmDirs;
    }

    /**
     * Activate the KeyboardManager application
     */
    public static void launchKM(final Activity activity) {
        final Intent kmIntent = new Intent()
                .setClassName(Constants.SYSTEM_APP_PACKAGE_NAME, Constants.KM_CLASS_NAME);

        boolean repeat;
        int numTry = 10;

        do {
            try {
                Thread.sleep(250);
                activity.startActivity(kmIntent);
                repeat = false;
            } catch (final Exception e) {
                Log.w(TAG, e);
                repeat = true;
            }
        } while (repeat && --numTry > 0);

        if ( repeat )
            Utils.toastMessage(activity, "Unable to start KM system app.");
    }

    /**
     * Parse linux's proc mounts file to retrieve information for the 'system' directory.
     *
     * @return 'system' directory mount arguments
     */
    public static String parseProcMounts() {
        final File procMounts = new File(Constants.PROC_MOUNTS_PATH);
        if ( !procMounts.exists() )
            return null;

        final String mountPattern = "(.*) " + Constants.SYSTEM_PATH + " .*";
        final Pattern p = Pattern.compile(mountPattern);
        Matcher m;

        // Parse each line of /proc/mounts until you find the 'system' directory mount description
        try {
            final BufferedReader br = new BufferedReader(new FileReader(procMounts));
            String line = "";

            while ((line = br.readLine()) != null) {
                m = p.matcher(line);
                if ( m.matches() ) {
                    br.close();
                    return m.group(1);
                }
            }

            br.close();
        } catch (final Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }

    public static void toastMessage(final Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.i(TAG, message);
    }

    /**
     * Private constructor to prevent class instantiation.
     * @since 2.11
     */
    private Utils(){}

}