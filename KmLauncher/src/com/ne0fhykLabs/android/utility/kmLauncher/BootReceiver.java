package com.ne0fhykLabs.android.utility.kmLauncher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Receiver listening for device boot up event, and verifying the system app is installed.
 * @author fhuya
 * @since 2.11
 */
public class BootReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isKMInstalled = settings.getBoolean(Constants.PREF_KM_IS_INSTALLED, false);
        if ( isKMInstalled ) {
            // Check if the system app is present.
            if ( !Utils.checkForApk() ) {
                // TODO: Notify the user that the system app is not installed (even though it should
                // be, and ask for reinstall.
            }
        }
    }
}