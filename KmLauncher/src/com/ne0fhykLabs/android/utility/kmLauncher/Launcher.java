/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.kmLauncher;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ne0fhykLabs.android.utility.kmLauncher.Utils.LauncherStatus;

import java.io.*;
import java.util.List;

import static com.ne0fhykLabs.android.utility.kmLauncher.Constants.APK_NAME;
import static com.ne0fhykLabs.android.utility.kmLauncher.Constants.BUSYBOX_FILENAME;

/**
 * Install the KeyboardManager application as system application. As of android
 * ICS (4.0) doing so, only requires the application's apk file to be dropped
 * into '/system/app'
 *
 * @author fhuya
 */
public class Launcher extends Activity {

    private static final String TAG = Launcher.class.getName();

    private File busyboxBin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final LinearLayout kmMenu = (LinearLayout) findViewById(R.id.kmMenuPref);
        kmMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Utils.checkForApk()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Launcher.this);
                    builder.setTitle("Install KM system app!")
                            .setMessage("Keyboard Manager must be installed as a system app, " +
                                    "in order to enable its functionality.\n\n"
                                    + "Once installed, it can ONLY BE UNINSTALLED from this menu," +
                                    " by using the 'Uninstall' button.\n")
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();
                                        }
                                    })
                            .setPositiveButton("Install KM!",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();
                                            installKM();
                                        }
                                    })
                            .create().show();
                }
                else{
                    Utils.launchKM(Launcher.this);
                }
            }
        });

        final TextView kmMenuSummary = (TextView) findViewById(R.id.kmMenuSummary);
        kmMenuSummary.setText("configure KM system app ( ver: " + APK_NAME + " ).");

        final LinearLayout uninstallButton = (LinearLayout) findViewById(R.id.uninstallKMPref);
        uninstallButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Utils.checkForOtherVersions().isEmpty()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Launcher.this);
                    builder.setTitle("Uninstall KM system app.")
                            .setMessage("KM system app will be uninstalled, disabling its functionality.")
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();
                                        }
                                    })
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();
                                            uninstallSystemApp();
                                        }
                                    })
                            .create().show();
                }
            }
        });

        updateUninstallSummary();
    }

    private void updateUninstallSummary() {
        final TextView uninstallSummary = (TextView) findViewById(R.id.uninstallKMSummary);
        final List<File> installedVersions = Utils.checkForOtherVersions();
        final StringBuilder summaryText = new StringBuilder("Installed version(s): ");
        if (installedVersions.size() == 0)
            summaryText.append("none");
        else {
            boolean first = true;
            for (final File installedVersion : installedVersions) {
                if (first)
                    first = false;
                else
                    summaryText.append(", ");
                summaryText.append(installedVersion.getName());
            }
        }
        uninstallSummary.setText(summaryText.toString());
    }

    /**
     * Remount the filesystem with write permissions
     */
    private void mountSystemRW(DataOutputStream os) throws Exception {
        String mountCmd = "mount -w -o remount ";
        String mountArgs = Utils.parseProcMounts();

        if (mountArgs == null) {
            // Use busybox if enable to retrieve the filesystem mount
            // information
            this.setupBusybox();

            os.writeBytes("chmod 777 ".concat(busyboxBin.getAbsolutePath()).concat("\n"));
            mountCmd = busyboxBin.getAbsolutePath().concat(" ").concat(mountCmd);
            mountArgs = "";
        }

        mountArgs = mountArgs.concat(" " + Constants.SYSTEM_PATH);

        Log.i(TAG, "Remounting " + Constants.SYSTEM_PATH + " with write permission");
        os.writeBytes(mountCmd.concat(mountArgs).concat("\n"));
    }

    /**
     * Upload busybox executable to this application data directory
     *
     * @throws IOException
     */
    private void setupBusybox() throws IOException {
        final File binDir = getDir("bin", Context.MODE_WORLD_READABLE);
        busyboxBin = new File(binDir, BUSYBOX_FILENAME);

        // Create bin directory in internal storage, and upload busybox there
        if (busyboxBin.exists())
            if (!busyboxBin.delete())
                Log.e(TAG, "Unable to delete ".concat(busyboxBin.getAbsolutePath()));

        final FileOutputStream fosBB = new FileOutputStream(busyboxBin);
        Log.i(TAG, "Writing helper libraries to internal storage");

        try {
            this.writeToDataDir(fosBB, "bin/".concat(BUSYBOX_FILENAME));
        } catch (final IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            fosBB.close();
        }
    }

    /**
     * Upload KeyboardManager apk to this application data directory
     *
     * @return String the path to KeyboardManager apk file.
     * @throws IOException
     */
    private String setupCoreApk() throws IOException {
        final String internalApk = "coreApk";
        final File internalApkFile = new File(getFilesDir(), internalApk);

        // Open handle to internal storage for the apk files
        if (internalApkFile.exists())
            if (!internalApkFile.delete())
                Log.e(TAG, "Unable to delete ".concat(internalApkFile.getAbsolutePath()));

        final FileOutputStream fosApk = openFileOutput(internalApk, Context.MODE_WORLD_READABLE);
        Log.i(TAG, "Writing core apk file to internal storage");

        try {
            this.writeToDataDir(fosApk, "app/".concat(APK_NAME));
        } catch (final IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            fosApk.close();
        }

        return internalApkFile.getAbsolutePath();
    }

    private void writeToDataDir(FileOutputStream fos, String assetLocation) throws IOException {
        final AssetManager assets = getAssets();

        final InputStream is = assets.open(assetLocation, AssetManager.ACCESS_BUFFER);

        final byte[] readBuffer = new byte[4096];
        int numBytes;
        while ((numBytes = is.read(readBuffer)) != -1)
            fos.write(readBuffer, 0, numBytes);

        fos.flush();
        fos.close();
        is.close();
    }

    private void uninstallSystemApp() {
        new SystemAppAsyncSetup(false).execute();
    }

    /**
     * Install KeyboardManager on the device
     */
    private void installKM() {
        new SystemAppAsyncSetup(true).execute();
    }

    private class SystemAppAsyncSetup extends AsyncTask<Void, Integer, LauncherStatus> {

        private ProgressDialog mProgress;
        private final boolean mInstallFlag;

        public SystemAppAsyncSetup(final boolean installFlag) {
            mInstallFlag = installFlag;
        }

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(Launcher.this);
            mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            final String message = mInstallFlag ? "Installing..." : "Uninstalling...";
            mProgress.setMessage(message);
            mProgress.setIndeterminate(false);
            mProgress.setCancelable(false);
            mProgress.setMax(8);
            mProgress.setProgress(0);
            mProgress.show();
        }

        @Override
        protected LauncherStatus doInBackground(Void... params) {
            if (this.isCancelled())
                return LauncherStatus.CANCELLED;

            // Check for other versions
            final List<File> otherVersions = Utils.checkForOtherVersions();

            if (mInstallFlag) {
                // If the app is already installed, cancel
                if (Utils.checkForApk())
                    return LauncherStatus.SUCCESS;
            }
            else {
                if (otherVersions.isEmpty())
                    return LauncherStatus.SUCCESS;
            }

            publishProgress(1);

            Process rootProcess;

            try {
                // Perform su to get root priviledges
                Log.i(TAG, "Requesting root priviledges");
                rootProcess = Runtime.getRuntime().exec("su");
                final DataOutputStream os = new DataOutputStream(rootProcess.getOutputStream());

                publishProgress(2);

                Log.i(TAG, "Configuring environment...");
                String internalApkPath = null;
                try {
                    if (mInstallFlag) {
                        internalApkPath = setupCoreApk();
                        publishProgress(3);
                    }

                    mountSystemRW(os);
                    publishProgress(4);

                } catch (final Exception e) {
                    Log.e(TAG, "Error occurred while remounting " + Constants.SYSTEM_PATH, e);
                    return LauncherStatus.FAILED_GENERIC_ERROR;
                }

                // Remove other versions
                if (otherVersions != null && !otherVersions.isEmpty()) {
                    Log.i(TAG, "Cleaning KM system app(s)");
                    for (final File file : otherVersions) {
                        final String filename = file.getAbsolutePath();
                        Log.i(TAG, "Removing ".concat(filename));
                        os.writeBytes("rm -r ".concat(filename).concat("\n"));
                    }
                }

                publishProgress(5);

                if (mInstallFlag) {
                    // Move binary file from internal storage to the system app directory
                    final String systemAppPath = Utils.getSystemAppPath();

                    os.writeBytes("dd if=" + internalApkPath + " of=" + systemAppPath +
                            "km_holder.tmp\n");
                    os.writeBytes("chmod 644 " + systemAppPath + "km_holder.tmp\n");
                    os.writeBytes("mv " + systemAppPath + "km_holder.tmp " + systemAppPath +
                            APK_NAME + "\n");

                    publishProgress(6);
                }

                // Close the terminal
                os.writeBytes("exit\n");
                os.flush();
                os.close();

                publishProgress(7);

                try {
                    if (rootProcess.waitFor() != 255)
                        rootProcess.destroy();
                    else {
                        rootProcess.destroy();
                        return LauncherStatus.FAILED_NO_ROOT_ACCESS;
                    }
                } catch (final InterruptedException e) {
                    Log.e(TAG,
                            "Interrupted exception occurred while waiting for shell process " +
                                    "completion",
                            e);
                    return LauncherStatus.FAILED_GENERIC_ERROR;
                }
            } catch (final IOException e) {
                Log.e(TAG, "IO exception occurred while running shell process", e);
                return LauncherStatus.FAILED_GENERIC_ERROR;
            }

            publishProgress(8);
            return LauncherStatus.SUCCESS;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            mProgress.setProgress(progress[0]);
        }

        protected void onCancelled(LauncherStatus status) {
            final String operationType = mInstallFlag ? "Install" : "Uninstall";
            final String cancelMessage = operationType + " operation was cancelled.";

            mProgress.setMessage(cancelMessage);
            mProgress.dismiss();
            Utils.toastMessage(getApplicationContext(), cancelMessage);
        }

        @Override
        protected void onPostExecute(LauncherStatus status) {
            mProgress.dismiss();

            final Activity activity = Launcher.this;

            final String operationType = mInstallFlag ? "Install " : "Uninstall ";
            final String successMessage = operationType + status.msg;
            Utils.toastMessage(activity, successMessage);

            final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences
                    (getApplicationContext()).edit();
            if (status == LauncherStatus.SUCCESS) {
                if (mInstallFlag) {
                    // Launch the KM app.
                    editor.putBoolean(Constants.PREF_KM_IS_INSTALLED, true).commit();
                    Utils.launchKM(activity);
                }
                else {
                    editor.putBoolean(Constants.PREF_KM_IS_INSTALLED, false).commit();
                }
            }

            updateUninstallSummary();
        }
    }
}
