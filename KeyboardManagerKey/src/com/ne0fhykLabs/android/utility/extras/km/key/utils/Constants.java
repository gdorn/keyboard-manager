/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.extras.km.key.utils;

import android.content.Intent;
import android.net.Uri;


public class Constants {

    private static final String PACKAGE_NAME = Constants.class.getPackage().getName();

    public static final int NOTIFICATION_ID = 101;
    public static final int PENDING_INTENT_REQUEST_CODE = 0;

    public static final String APP_PACKAGE_NAME_KEY = "app_package_name_key";

    public static final String INSTALLER_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.kmLauncher";
    public static final int INSTALLER_LEAST_VERSION_CODE = 15;
    public static final String INSTALLER_LEAST_VERSION_NAME = "2.6";

    public static final String SERVICE_BUNDLE_KEY = "com.ne0fhykLabs.android.utility.km.serviceBundle";
    public static final String SYSTEM_APP_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.km";

    public static final Intent INTENT_INSTALL_KM = new Intent(Intent.ACTION_VIEW, Uri
            .parse("market://details?id=" + Constants.INSTALLER_PACKAGE_NAME));

    /**
     * Intent actions
     */
    public static final String ACTION_INSTALL_KM = PACKAGE_NAME + ".action_install_km";
    public static final String ACTION_UPDATE_KM = PACKAGE_NAME + ".action_update_km";
    public static final String ACTION_KM_PROVIDER_MISSING = PACKAGE_NAME + ".km_provider_missing";

    /**
     * Provider constants
     */
    public static final String PROVIDER_AUTHORITY = "com.ne0fhykLabs.android.utility.km.KMConfigurationProvider";
    public static final Uri PROVIDER_URI = Uri.parse("content://" + PROVIDER_AUTHORITY);
    public static final Uri CONTENT_URI = Uri.withAppendedPath(PROVIDER_URI, "current_app");
    public static final String CONTENT_MIME_TYPE = "text/plain";
    public static final String CURRENT_PACKAGE_NAME = "current_package_name";

    /**
     * Shared preferences constants
     */
    public static final String PREF_KM_PROVIDER_MISSING_SHOW_AGAIN = "km_provider_missing_show_again";
    public static final boolean DEFAULT_KM_PROVIDER_MISSING_SHOW_AGAIN = true;

    public static final String PREF_INSTALL_KM_SHOW_AGAIN = "install_km_show_again";
    public static final boolean DEFAULT_INSTALL_KM_SHOW_AGAIN = true;

    public static final String PREF_UPDATE_KM_SHOW_AGAIM = "update_km_show_again";
    public static final boolean DEFAULT_UPDATE_KM_SHOW_AGAIN = true;

}
