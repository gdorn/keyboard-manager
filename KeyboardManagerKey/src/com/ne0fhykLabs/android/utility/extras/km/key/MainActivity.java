/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.extras.km.key;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ne0fhykLabs.android.utility.extras.km.key.utils.Constants;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        handleIntent(intent);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if ( Constants.ACTION_INSTALL_KM.equals(action) ) {
            installKMDialog();
        }
        else if ( Constants.ACTION_UPDATE_KM.equals(action) ) {
            updateKMDialog();
        }
        else if ( Constants.ACTION_KM_PROVIDER_MISSING.equals(action) ) {
            informKMProviderMissing();
        }
    }

    private void setNegativeButton(String buttonText, View.OnClickListener buttonListener) {
        Button button = (Button) findViewById(R.id.ma_negative_button);
        button.setText(buttonText);
        button.setOnClickListener(buttonListener);
        button.setVisibility(View.VISIBLE);
    }

    private void setNeutralButton(String buttonText, View.OnClickListener buttonListener) {
        Button button = (Button) findViewById(R.id.ma_neutral_button);
        button.setText(buttonText);
        button.setOnClickListener(buttonListener);
        button.setVisibility(View.VISIBLE);
    }

    private void setPositiveButton(String buttonText, View.OnClickListener buttonListener) {
        Button button = (Button) findViewById(R.id.ma_positive_button);
        button.setText(buttonText);
        button.setOnClickListener(buttonListener);
        button.setVisibility(View.VISIBLE);
    }

    private void setDialogMsg(int msgId) {
        TextView msgView = (TextView) findViewById(R.id.ma_dialog_msg);
        msgView.setText(msgId);
    }

    private void configureShowAgain(final String prefKey, boolean defaultValue) {
        CheckBox showAgainCheck = (CheckBox) findViewById(R.id.ma_dialog_show_again_switch);
        showAgainCheck.setVisibility(View.VISIBLE);

        final SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        boolean checkValue = settings.getBoolean(prefKey, defaultValue);

        showAgainCheck.setChecked(checkValue);
        showAgainCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                settings.edit().putBoolean(prefKey, isChecked).commit();
            }
        });
    }

    private void informKMProviderMissing() {

        setTitle(R.string.title_km_provider_missing_dialog);
        setDialogMsg(R.string.text_km_provider_missing_dialog);

        configureShowAgain(Constants.PREF_KM_PROVIDER_MISSING_SHOW_AGAIN,
                           Constants.DEFAULT_KM_PROVIDER_MISSING_SHOW_AGAIN);

        setNeutralButton("Ok", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void installKMDialog() {
        setTitle(R.string.title_install_km_dialog);
        setDialogMsg(R.string.text_install_km_dialog);

        configureShowAgain(Constants.PREF_INSTALL_KM_SHOW_AGAIN,
                           Constants.DEFAULT_INSTALL_KM_SHOW_AGAIN);

        setNegativeButton("Cancel", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setNeutralButton("Disable KM Key.", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                finish();
            }
        });

        setPositiveButton("Install KM!", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + Constants.INSTALLER_PACKAGE_NAME)));
                finish();
            }
        });
    }

    private void updateKMDialog() {
        setTitle(R.string.title_update_km_dialog);
        setDialogMsg(R.string.text_update_km_dialog);

        configureShowAgain(Constants.PREF_UPDATE_KM_SHOW_AGAIM,
                           Constants.DEFAULT_UPDATE_KM_SHOW_AGAIN);

        setNegativeButton("Cancel", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setNeutralButton("Disable KM Key.", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                finish();
            }
        });

        setPositiveButton("Update KM!", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + Constants.INSTALLER_PACKAGE_NAME)));
                finish();
            }
        });
    }
}
