/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.extras.km.key;

import android.accessibilityservice.AccessibilityService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.ne0fhykLabs.android.utility.extras.km.key.utils.Constants;
import com.ne0fhykLabs.android.utility.extras.km.key.utils.Utils;
import com.ne0fhykLabs.android.utility.extras.km.key.utils.Utils.KMState;

public class AppSwitchDetector extends AccessibilityService {

    private static final String TAG = AppSwitchDetector.class.getName();

    private static boolean sReturnFromActivity = false;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.accessibilityservice.AccessibilityService#onAccessibilityEvent(android.view.accessibility
     * .AccessibilityEvent)
     */
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if ( sReturnFromActivity ) {
            sReturnFromActivity = false;
            return;
        }

        String className = event.getClassName().toString();
        // Ignore our own activity class
        if ( MainActivity.class.getName().equals(className) ) {
            sReturnFromActivity = true;
            return;
        }

        final Context context = getApplicationContext();
        final KMState kmState = Utils.checkForKMLauncher(context);

        final NotificationManager notificationManager = (NotificationManager) getSystemService
                (Context.NOTIFICATION_SERVICE);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setOngoing(true);
        final PendingIntent installKM = PendingIntent.getActivity(context,
                Constants.PENDING_INTENT_REQUEST_CODE, Constants.INTENT_INSTALL_KM,
                PendingIntent.FLAG_UPDATE_CURRENT);


        if ( kmState == KMState.NOT_INSTALLED ) {
            builder.setContentTitle(getString(R.string.title_install_km_dialog))
                    .setContentText(getString(R.string.text_install_km_dialog))
                    .setContentIntent(installKM);

            notificationManager.notify(Constants.NOTIFICATION_ID, builder.build());
            return;
        }
        else if ( kmState == KMState.NOT_UP_TO_DATE ) {
            builder.setContentTitle(getString(R.string.title_update_km_dialog))
                    .setContentText(getString(R.string.text_update_km_dialog))
                    .setContentIntent(installKM);

            notificationManager.notify(Constants.NOTIFICATION_ID, builder.build());
            return;
        }

        // Check to see if the km provider has been detected by the system
        final ContentResolver cr = getContentResolver();

        String type = cr.getType(Constants.CONTENT_URI);
        if ( !Constants.CONTENT_MIME_TYPE.equals(type) ) {
            builder.setContentTitle(getString(R.string.title_km_provider_missing_dialog))
                    .setContentText(getString(R.string.text_km_provider_missing_dialog));

            notificationManager.notify(Constants.NOTIFICATION_ID, builder.build());
            return;
        }

        notificationManager.cancel(Constants.NOTIFICATION_ID);

        // Send the event information to the KeyboardSwitcher service.
        String packageName = event.getPackageName().toString();
        Log.d(TAG, "Detected package: " + packageName);

        ContentValues cv = new ContentValues(1);
        cv.put(Constants.CURRENT_PACKAGE_NAME, packageName);
        cr.insert(Constants.CONTENT_URI, cv);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.accessibilityservice.AccessibilityService#onInterrupt()
     */
    @Override
    public void onInterrupt() {
        // Nothing to do
    }

}
