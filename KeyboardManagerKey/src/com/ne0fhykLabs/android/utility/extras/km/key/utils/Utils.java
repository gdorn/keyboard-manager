/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.extras.km.key.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;

public class Utils {

    public enum KMState {
        NOT_INSTALLED,
        NOT_UP_TO_DATE,
        UP_TO_DATE
    }

    public static boolean checkForSystemApp(Context context) {
        try {
            context.getPackageManager().getPackageInfo(Constants.SYSTEM_APP_PACKAGE_NAME, 0);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public static KMState checkForKMLauncher(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager()
                    .getPackageInfo(Constants.INSTALLER_PACKAGE_NAME, 0);
            if ( pInfo.versionCode >= Constants.INSTALLER_LEAST_VERSION_CODE )
                return KMState.UP_TO_DATE;

            return KMState.NOT_UP_TO_DATE;
        } catch (NameNotFoundException e) {
            return KMState.NOT_INSTALLED;
        }
    }

    public static void toastMessage(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void toastMessage(Context context, int msgId) {
        Toast.makeText(context, msgId, Toast.LENGTH_LONG).show();
    }
}
