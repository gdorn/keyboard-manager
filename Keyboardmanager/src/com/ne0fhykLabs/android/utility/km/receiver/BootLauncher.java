/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.receiver;

import static com.ne0fhykLabs.android.utility.km.utils.Constants.BOOT_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_NAME;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_PERMS;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TICKER_INTENT_KEY;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TOGGLE_STATE;

import com.ne0fhykLabs.android.utility.km.service.KeyboardSwitcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Listener for the BOOT_COMPLETED broadcast.
 * Starts the service if the setting 'Start on boot' was enabled
 *
 * @author ne0fhyk
 *
 */
public class BootLauncher extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent){
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, PREFS_PERMS);

		boolean startOnBoot = settings.getBoolean(BOOT_STATE, false);
		boolean start = settings.getBoolean(TOGGLE_STATE, false);

		if(startOnBoot && start){
			Intent serviceIntent = new Intent(context, KeyboardSwitcher.class);
			serviceIntent.putExtra(TICKER_INTENT_KEY, "Starting Keyboard Manager...");
			context.startService(serviceIntent);
		}
	}
}
