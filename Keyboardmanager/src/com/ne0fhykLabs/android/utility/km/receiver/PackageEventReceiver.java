/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.receiver;

import static com.ne0fhykLabs.android.utility.km.utils.Constants.INSTALLER_PACKAGE_NAME;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.ne0fhykLabs.android.utility.km.service.KeyboardSwitcher;
import com.ne0fhykLabs.android.utility.km.utils.Constants;
import com.ne0fhykLabs.android.utility.km.utils.Utils;

/**
 * Given Keyboard Manager is a system application, it cannot be removed through
 * the application management interface.
 *
 * This class monitors the removal of 'Keyboard Manager' installer, and
 * companion application, and disable the application.
 *
 * @author ne0fhyk
 *
 */
public class PackageEventReceiver extends BroadcastReceiver {

    private static final String TAG = PackageEventReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        String packageName = data.getSchemeSpecificPart();

        Intent serviceIntent = new Intent(context, KeyboardSwitcher.class);
        if (packageName.equals(INSTALLER_PACKAGE_NAME)) {
            // Disable the keyboard manager service
            Log.i(TAG, "KM Launcher was uninstalled. Keyboard Manager will be disabled");
            Utils.disableKM(context);

            context.stopService(serviceIntent);
        } else if (packageName.equals(Constants.KM_KEY_PACKAGE_NAME)) {
            // Tell the service to check for the key
            context.startService(serviceIntent);
        }

    }

}
