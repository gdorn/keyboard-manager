/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.activity;

import static com.ne0fhykLabs.android.utility.km.utils.Constants.BACKUP_FILEPATH_PREF;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.BACKUP_PREF;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.BOOT_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.DEFAULT_BACKUP_FILENAME;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.ICON_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.KILL_PREV_IME;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_NAME;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_PERMS;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.RESTORE_PREF;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.SERVICE_BUNDLE_KEY;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TICKER_INTENT_KEY;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TOGGLE_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TRANSITION_SCREEN_STATE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.util.Xml;

import com.ne0fhykLabs.android.utility.km.R;
import com.ne0fhykLabs.android.utility.km.service.KeyboardSwitcher;
import com.ne0fhykLabs.android.utility.km.utils.Constants;
import com.ne0fhykLabs.android.utility.km.utils.KeyboardPreferences;
import com.ne0fhykLabs.android.utility.km.utils.Utils;
import com.ne0fhykLabs.android.utility.km.utils.Utils.KMKeyState;

/**
 * Primary user interface Configures the settings, and start/stop the Keyboard
 * Manager service
 *
 * @author ne0fhyk
 */
public class KeyboardManager extends PreferenceActivity {

    private static final String TAG = KeyboardManager.class.getName();

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Uri data = intent.getData();
            String packageName = data.getSchemeSpecificPart();
            if ( packageName.equals(Constants.KM_KEY_PACKAGE_NAME) )
                updateAppScreen();
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preference);

        // Customize preference settings
        PreferenceManager prefManager = getPreferenceManager();
        prefManager.setSharedPreferencesName(PREFS_NAME);
        prefManager.setSharedPreferencesMode(PREFS_PERMS);

        this.setupPreferenceScreen();
    }

    @SuppressLint("NewApi")
    private File getExternalFilesDir() {
        int version = android.os.Build.VERSION.SDK_INT;
        if ( version >= 8 )
            return getExternalFilesDir(null);
        else {
            return new File(Environment.getExternalStorageDirectory(),
                    "/Android/data/" + getPackageName() + "/files/");
        }
    }

    // TODO: complete case for Set<String> preference
    private boolean importPreferences(File importFile) {
        boolean result = true;
        FileReader in = null;

        final String namespace = null;
        final String nameAttr = "name";
        final String valueAttr = "value";

        // Initialize the variables to store the data from the xml file.
        final class NameValuePair {
            final String name;
            final String value;

            NameValuePair(String name, String value) {
                this.name = name;
                this.value = value;
            }

            @Override
            public String toString() {
                return "[" + name + " => " + value + "]";
            }
        }

        final Map<String, List<NameValuePair>> importContents = new HashMap<String, List<NameValuePair>>();

        final XmlPullParser parser = Xml.newPullParser();
        try {
            in = new FileReader(importFile);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in);
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, namespace, "map");

            for ( int eventType = parser.next(); eventType != XmlPullParser.END_DOCUMENT; eventType = parser
                    .next() ) {
                if ( eventType == XmlPullParser.START_TAG ) {
                    String tagName = parser.getName();
                    if ( tagName == null )
                        continue;

                    String name = parser.getAttributeValue(namespace, nameAttr);
                    String value = tagName.equals("string") ? parser.nextText()
                            : parser.getAttributeValue(namespace,
                                    valueAttr);
                    NameValuePair pair = new NameValuePair(name, value);

                    List<NameValuePair> tagContents = importContents.get(tagName);
                    if ( tagContents == null )
                        tagContents = new ArrayList<NameValuePair>();

                    tagContents.add(pair);
                    importContents.put(tagName, tagContents);
                }
            }

        } catch (XmlPullParserException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Unable to locate restore file.", e);
            Utils.toastMessage(getApplicationContext(), "Unable to locate restore file.");
            result = false;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        } finally {
            try {
                if ( in != null )
                    in.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        if ( importContents.size() > 0 ) {
            final SharedPreferences.Editor editor = getPreferenceManager().getSharedPreferences()
                    .edit();

            // Clear the previous preferences
            editor.clear();

            for ( Map.Entry<String, List<NameValuePair>> prefEntry : importContents.entrySet() ) {
                String tagName = prefEntry.getKey();
                List<NameValuePair> prefPairList = prefEntry.getValue();

                for ( NameValuePair prefPair : prefPairList ) {
                    String prefKey = prefPair.name;
                    String prefValue = prefPair.value;

                    if ( tagName == null )
                        continue;
                    else if ( tagName.equals("boolean") ) {
                        editor.putBoolean(prefKey, Boolean.parseBoolean(prefValue));
                    }
                    else if ( tagName.equals("float") ) {
                        editor.putFloat(prefKey, Float.parseFloat(prefValue));
                    }
                    else if ( tagName.equals("int") ) {
                        editor.putInt(prefKey, Integer.parseInt(prefValue));
                    }
                    else if ( tagName.equals("long") ) {
                        editor.putLong(prefKey, Long.parseLong(prefValue));
                    }
                    else if ( tagName.equals("string") ) {
                        editor.putString(prefKey, prefValue);
                    }
                }
            }

            // Commit the changes
            result = editor.commit();
        }

        return result;
    }

    // TODO: complete case for Set<String> preference
    private void writePreferenceToXml(final XmlSerializer initializedSerializer,
            final String namespace, String prefKey, Object prefValue)
            throws IOException {

        final String nameAttr = "name";
        final String valueAttr = "value";

        if ( prefValue instanceof Boolean ) {
            Boolean value = (Boolean) prefValue;
            initializedSerializer.startTag(namespace, "boolean");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "boolean");
        }
        else if ( prefValue instanceof Float ) {
            Float value = (Float) prefValue;
            initializedSerializer.startTag(namespace, "float");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "float");
        }
        else if ( prefValue instanceof Integer ) {
            Integer value = (Integer) prefValue;
            initializedSerializer.startTag(namespace, "int");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "int");
        }
        else if ( prefValue instanceof Long ) {
            Long value = (Long) prefValue;
            initializedSerializer.startTag(namespace, "long");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.attribute(namespace, valueAttr, value.toString());
            initializedSerializer.endTag(namespace, "long");
        }
        else if ( prefValue instanceof String ) {
            String value = (String) prefValue;
            initializedSerializer.startTag(namespace, "string");
            initializedSerializer.attribute(namespace, nameAttr, prefKey);
            initializedSerializer.text(value);
            initializedSerializer.endTag(namespace, "string");
        }
    }

    private boolean exportPreferences(File exportDestination) {
        boolean result = true;
        FileWriter out = null;

        final String namespace = null;

        final XmlSerializer serializer = Xml.newSerializer();
        final SharedPreferences settings = getPreferenceManager().getSharedPreferences();
        final Map<String, ?> settingsContent = settings.getAll();

        try {
            out = new FileWriter(exportDestination);

            serializer.setOutput(out);
            serializer.startDocument("UTF-8", true);
            serializer.startTag(namespace, "map");

            for ( Map.Entry<String, ?> prefEntry : settingsContent.entrySet() ) {
                String prefKey = prefEntry.getKey();
                Object prefValue = prefEntry.getValue();

                writePreferenceToXml(serializer, namespace, prefKey, prefValue);
            }

            serializer.endTag(namespace, "map");
            serializer.endDocument();

        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            result = false;
        } finally {
            try {
                if ( out != null )
                    out.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }

        return result;
    }

    private void setupPreferenceScreen() {
        // Customize preference settings
        PreferenceManager prefManager = getPreferenceManager();

        final SharedPreferences settings = prefManager.getSharedPreferences();

        // Retrieve the preference from preference.xml
        CheckBoxPreference startStopPref = (CheckBoxPreference) findPreference(TOGGLE_STATE);
        startStopPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        boolean startDefValue = settings.getBoolean(TOGGLE_STATE, false);
        startStopPref.setChecked(startDefValue);

        KeyboardPreferences keyboardPref =
                (KeyboardPreferences) findPreference(Constants.GLOBAL_KEYBOARD_PREF_KEY);
        keyboardPref.setOnPreferenceChangeListener(mPreferenceChangeListener);

        CheckBoxPreference startOnBootPref = (CheckBoxPreference) findPreference(BOOT_STATE);
        boolean startOnBootFlag = settings.getBoolean(BOOT_STATE, false);
        startOnBootPref.setChecked(startOnBootFlag);

        // Utility preferences
        String defaultBackupFilepath =
                new File(getExternalFilesDir(), DEFAULT_BACKUP_FILENAME).toString();
        String backupFilepath = settings.getString(BACKUP_FILEPATH_PREF, defaultBackupFilepath);

        EditTextPreference backupPref = (EditTextPreference) findPreference(BACKUP_PREF);
        backupPref.setText(backupFilepath);
        backupPref.setTitle("Backup preferences");
        backupPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Context context = getApplicationContext();

                // Store the application preferences to the path in newValue
                String backupPath = (String) newValue;

                settings.edit().putString(BACKUP_FILEPATH_PREF, backupPath).commit();

                File backupFile = new File(backupPath);
                File parentDir = backupFile.getParentFile();
                if ( !parentDir.mkdirs() && !parentDir.isDirectory() ) {
                    Utils.toastMessage(context, "Unable to create backup directory.");
                    return false;
                }

                // Copy the shared preferences file to the backup location
                boolean result = exportPreferences(backupFile);
                if ( result )
                    Utils.toastMessage(context, "Backup complete!");
                else
                    Utils.toastMessage(context, "Backup failed!");

                return result;
            }
        });

        EditTextPreference restorePref = (EditTextPreference) findPreference(RESTORE_PREF);
        restorePref.setText(backupFilepath);
        restorePref.setTitle("Restore preferences");
        restorePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Context context = getApplicationContext();

                String restorePath = (String) newValue;

                File restoreFile = new File(restorePath);
                if ( !restoreFile.exists() ) {
                    Utils.toastMessage(context, "Unable to locate preferences backup.");
                    return false;
                }

                boolean result = importPreferences(restoreFile);
                if ( result ) {
                    settings.edit().putString(BACKUP_FILEPATH_PREF, restorePath).commit();

                    // Update the preference screen
                    setupPreferenceScreen();
                    toggleKm();
                    updateAppScreen();

                    Utils.toastMessage(context, "Restore complete!");
                }
                else
                    Utils.toastMessage(context, "Restore failed!");

                return result;
            }
        });

        // Advanced preferences
        CheckBoxPreference transitionScreenPref =
                (CheckBoxPreference) findPreference(TRANSITION_SCREEN_STATE);
        transitionScreenPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        boolean transitionScreenFlag = settings.getBoolean(TRANSITION_SCREEN_STATE, false);
        transitionScreenPref.setChecked(transitionScreenFlag);

        CheckBoxPreference killPrevImePref = (CheckBoxPreference) findPreference(KILL_PREV_IME);
        killPrevImePref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        boolean killPrevImeFlag = settings.getBoolean(KILL_PREV_IME, false);
        killPrevImePref.setChecked(killPrevImeFlag);

        CheckBoxPreference notificationIconPref = (CheckBoxPreference) findPreference(ICON_STATE);
        notificationIconPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        boolean notificationDefValue = settings.getBoolean(ICON_STATE, false);
        notificationIconPref.setChecked(notificationDefValue);

    }

    @Override
    public void onStart() {
        super.onStart();

        toggleKm();
    }

    private void toggleKm() {
        // Start or stop KeyboardSwitcher service based on startStopPref state
        CheckBoxPreference startStopPref = (CheckBoxPreference) findPreference(TOGGLE_STATE);

        Intent serviceIntent = new Intent(this, KeyboardSwitcher.class);
        if ( startStopPref.isChecked() ) {
            startService(serviceIntent);
        }
        else {
            stopService(serviceIntent);
        }
    }

    public void updateAppScreen() {
        final Context context = getApplicationContext();
        final KMKeyState keyState = Utils.checkForKey(context);

        PreferenceScreen appsPrefScreen =
                (PreferenceScreen) findPreference(Constants.APPS_KEYBOARD_PREF_SCREEN_KEY);
        appsPrefScreen.removeAll();

        if ( keyState == KMKeyState.UP_TO_DATE ) {
            final boolean isKMKeyEnabled = Utils.isAccessibilityServiceEnabled(context);
            if ( !isKMKeyEnabled ) {
                appsPrefScreen.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        // Show a dialog asking them to go enable KM Extras
                        Utils.enableAccessibilityServiceDialog(KeyboardManager.this);
                        return true;

                    }
                });
            }
            else {
                appsPrefScreen.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        return false;

                    }
                });

                Preference addAppsPref = new Preference(context);
                addAppsPref.setKey(Constants.ADD_APPS_PREF_KEY);
                addAppsPref.setTitle("Select apps");
                addAppsPref.setSummary("click to add/remove apps.");
                addAppsPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Utils.selectAppDialog(KeyboardManager.this);
                        return true;
                    }
                });

                appsPrefScreen.addPreference(addAppsPref);

                // Load the list of enabled apps, and add it to the preference
                // screen
                final Map<String, ComponentName> appsInfo = Utils.getEnabledAppList(context);

                for ( Map.Entry<String, ComponentName> entry : appsInfo.entrySet() ) {
                    KeyboardPreferences appPreference = new KeyboardPreferences(this, null);
                    String appName = entry.getKey();
                    appPreference.setTitle(appName);
                    appPreference.setOnPreferenceChangeListener(mPreferenceChangeListener);
                    appPreference.setSummary("Keyboard preferences for " + appName);
                    appPreference.setKey(Constants.APP_KEYBOARD_PREF_KEY + "."
                                         + entry.getValue().flattenToShortString());

                    appsPrefScreen.addPreference(appPreference);
                }
            }
        }
        else {
            appsPrefScreen.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if ( keyState == KMKeyState.NOT_INSTALLED )
                        Utils.installKMKeyDialog(KeyboardManager.this);
                    else if ( keyState == KMKeyState.NOT_UP_TO_DATE )
                        Utils.updateKMKeyDialog(KeyboardManager.this);
                    else
                        return false;
                    return true;
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        updateAppScreen();

        // Set up a broadcast receiver for the key (un)install
        IntentFilter keyFilter = new IntentFilter();
        keyFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        keyFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        keyFilter.addDataScheme("package");
        registerReceiver(mBroadcastReceiver, keyFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
    }

    private final OnPreferenceChangeListener mPreferenceChangeListener =
            new KeyboardPreferenceChangeListener();

    public class KeyboardPreferenceChangeListener implements OnPreferenceChangeListener {

        public boolean onPreferenceChange(String prefKey, Object newValue) {
            Intent serviceIntent = new Intent(KeyboardManager.this,
                    KeyboardSwitcher.class);

            if ( prefKey.equals(TOGGLE_STATE) ) {
                Boolean actualValue = (Boolean) newValue;
                if ( actualValue.booleanValue() ) {
                    startService(serviceIntent);
                }
                else {
                    stopService(serviceIntent);
                }
            }
            else {
                String tickerText = "";
                Bundle serviceBundle = new Bundle(1);

                SharedPreferences settings = getSharedPreferences(PREFS_NAME, PREFS_PERMS);
                boolean iconFlag = settings.getBoolean(ICON_STATE, false);

                if ( prefKey.startsWith(Constants.KEYBOARD_PREF_PREFIX) ) {

                    String[] keyParts = prefKey.split("\\.");
                    int partCount = keyParts.length;
                    if ( partCount < 3 ) {
                        Log.w(TAG, "Missing preference key information ( "
                                   + prefKey + " ).");
                        return false;
                    }

                    String configurationName = keyParts[partCount - 1];
                    tickerText = "Updating " + configurationName.toLowerCase(Locale.US)
                                 + " keyboard settings...";
                    String keyboardId = (String) newValue;
                    serviceBundle.putString(prefKey, keyboardId);

                }
                else {
                    if ( prefKey.equals(ICON_STATE) ) {
                        iconFlag = ((Boolean) newValue).booleanValue();
                        tickerText = "Updating notification settings...";
                        serviceBundle.putBoolean(prefKey, iconFlag);

                    }
                    else if ( prefKey.equals(TRANSITION_SCREEN_STATE) ) {
                        tickerText = "Updating transition screen settings...";
                        boolean transitionAllowed = ((Boolean) newValue).booleanValue();
                        serviceBundle.putBoolean(prefKey, transitionAllowed);

                    }
                    else if ( prefKey.equals(KILL_PREV_IME) ) {
                        tickerText = "Updating keyboard settings...";
                        boolean killImeFlag = ((Boolean) newValue).booleanValue();
                        serviceBundle.putBoolean(prefKey, killImeFlag);
                    }
                }

                serviceIntent.putExtra(SERVICE_BUNDLE_KEY, serviceBundle)
                        .putExtra(
                                TICKER_INTENT_KEY, tickerText);
                startService(serviceIntent);
            }
            return true;
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String prefKey = preference.getKey();
            return onPreferenceChange(prefKey, newValue);
        }

    }
}
