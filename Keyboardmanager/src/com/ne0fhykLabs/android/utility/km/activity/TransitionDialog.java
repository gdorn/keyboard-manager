/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.activity;

import com.ne0fhykLabs.android.utility.km.R;
import com.ne0fhykLabs.android.utility.km.R.layout;

import android.app.Activity;
import android.os.Bundle;

/**
 * Used to allow smooth transition between IME.
 * Experimentation showed that quick automatic switching the IMEs through Keyboard Manager on the
 * foreground activity without some kind of transition (switching to another activity/dialog and back)
 * would often cause the IME to appear in an invalid/unusable state.
 *
 * @author ne0fhyk
 *
 */
public class TransitionDialog extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public void onResume(){
        super.onResume();
        finish();
    }

    @Override
    public void onPause(){
        super.onPause();
        this.finish();
    }

}
