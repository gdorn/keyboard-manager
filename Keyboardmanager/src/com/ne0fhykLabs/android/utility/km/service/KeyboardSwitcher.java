/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.service;

import static com.ne0fhykLabs.android.utility.km.utils.Constants.ICON_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_NAME;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.PREFS_PERMS;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.ROOT_ACCESS;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.SERVICE_BUNDLE_KEY;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TICKER_INTENT_KEY;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TOGGLE_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Constants.TRANSITION_SCREEN_STATE;
import static com.ne0fhykLabs.android.utility.km.utils.Utils.acquireRoot;
import static com.ne0fhykLabs.android.utility.km.utils.Utils.toastMessage;

import java.util.HashMap;
import java.util.Map;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.ne0fhykLabs.android.utility.km.R;
import com.ne0fhykLabs.android.utility.km.activity.KeyboardManager;
import com.ne0fhykLabs.android.utility.km.activity.TransitionDialog;
import com.ne0fhykLabs.android.utility.km.utils.Constants;
import com.ne0fhykLabs.android.utility.km.utils.Utils;
import com.ne0fhykLabs.android.utility.km.utils.Utils.ConfigurationType;
import com.ne0fhykLabs.android.utility.km.utils.Utils.KMKeyState;

/**
 * Service in charge of switching the IME during orientation change
 *
 * @author ne0fhyk
 */
public class KeyboardSwitcher extends Service {

    private static final String TAG = KeyboardSwitcher.class.getName();

    enum DockState {
        CAR,
        DESK,
        UNDOCKED
    }

    protected final static int NOTIFICATION_ID = 1;
    protected final static boolean DEFAULT_TRANSITION_FLAG = false;
    protected final static boolean DEFAULT_KILL_IME_FLAG = false;

    protected final Map<String, String> mImePerConfig = new HashMap<String, String>();
    protected Map<String, ComponentName> mEnabledApps;

    protected Notification mNotification;

    // Keyboard related settings
    protected String mCurrId = "";

    protected boolean mTransitionAllowed;
    protected boolean mKillPrevIme;
    protected boolean mIsKeyEnabled;

    protected final IntentFilter mDockFilter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
    protected final BroadcastReceiver mDockEventReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ( Intent.ACTION_DOCK_EVENT.equals(action) ) {
                DockState dockState = getDockStateHelper(intent);

                // Update the current configuration
                configurationUpdate(getResources().getConfiguration(), true, dockState, null);
            }
        }
    };

    private String getCurrentAppCmp(final String packageName) {
        final PackageManager pm = getPackageManager();

        try {
            if ( packageName == null ) {
                ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                RunningTaskInfo startingActivity = am.getRunningTasks(1).get(0);
                if ( startingActivity == null ) {
                    Log.d(TAG, "Unable to retrieve starting activity.");
                    return null;
                }

                ComponentName actCmp = startingActivity.baseActivity;
                if ( actCmp == null ) {
                    Log.d(TAG, "Unable to retrieve starting activity component name.");
                    return null;
                }

                ActivityInfo info = pm.getActivityInfo(actCmp, 0);
                String appTitle = info.loadLabel(pm).toString();
                return appTitle;
            }
            else {
                ApplicationInfo appInfo = pm.getApplicationInfo(packageName, 0);
                String appTitle = appInfo.loadLabel(pm).toString();
                return appTitle;
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Unable to retrieve application info.");
            return null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, PREFS_PERMS);

        // Check for root access
        if ( !settings.getBoolean(ROOT_ACCESS, false) ) {
            if ( !acquireRoot(this) ) {
                Log.e(TAG, "Aborting Keyboard Manager service");
                toastMessage(this, "Aborting Keyboard Manager service");

                this.stopSelf();
            }

            SharedPreferences.Editor edit = settings.edit();
            edit.putBoolean(ROOT_ACCESS, true);
            edit.commit();
        }

        mTransitionAllowed = settings.getBoolean(TRANSITION_SCREEN_STATE,
                DEFAULT_TRANSITION_FLAG);
        mKillPrevIme = settings.getBoolean(Constants.KILL_PREV_IME, DEFAULT_KILL_IME_FLAG);

        loadEnabledAppList(settings);
        loadKeyboardConfigurations(settings);

        // Convert the old preferences into the new format.
        convertKeyboardPreferences();

        // Register broadcast receiver for docking event
        registerReceiver(mDockEventReceiver, mDockFilter);

    }

    private DockState getDockState() {
        Intent dockStatus = registerReceiver(null, mDockFilter);
        return getDockStateHelper(dockStatus);
    }

    private DockState getDockStateHelper(Intent dockIntent) {
        if ( dockIntent == null )
            return DockState.UNDOCKED;

        int dockState = dockIntent.getIntExtra(Intent.EXTRA_DOCK_STATE,
                Intent.EXTRA_DOCK_STATE_UNDOCKED);

        if ( android.os.Build.VERSION.SDK_INT >= 11 ) {
            if ( dockState == Intent.EXTRA_DOCK_STATE_LE_DESK ||
                 dockState == Intent.EXTRA_DOCK_STATE_HE_DESK )
                return DockState.DESK;
        }

        switch (dockState) {
            case Intent.EXTRA_DOCK_STATE_CAR:
                return DockState.CAR;

            case Intent.EXTRA_DOCK_STATE_DESK:
                return DockState.DESK;

            default:
            case Intent.EXTRA_DOCK_STATE_UNDOCKED:
                return DockState.UNDOCKED;

        }
    }

    private void loadEnabledAppList(SharedPreferences settings) {
        mEnabledApps = Utils.getEnabledAppList(getApplicationContext());
    }

    private void loadKeyboardConfigurations(SharedPreferences settings) {
        Map<String, ?> allPreferences = settings.getAll();
        for ( Map.Entry<String, ?> pref : allPreferences.entrySet() ) {
            String prefKey = pref.getKey();
            if ( prefKey.startsWith(Constants.KEYBOARD_PREF_PREFIX) ) {
                String keyboardId = (String) pref.getValue();
                mImePerConfig.put(prefKey, keyboardId);
            }
        }
    }

    private void convertKeyboardPreferences() {
        final String newHardwareKey = Utils.getGlobalKeyboardIdKey(ConfigurationType.HARDWARE);
        final String oldHardwareKey = "hardkeyboard-pref";
        updateKeyboardPreference(oldHardwareKey, newHardwareKey);

        final String newLandscapeKey = Utils.getGlobalKeyboardIdKey(ConfigurationType.LANDSCAPE);
        final String oldLandscapeKey = "landscape-pref";
        updateKeyboardPreference(oldLandscapeKey, newLandscapeKey);

        final String newPortraitKey = Utils.getGlobalKeyboardIdKey(ConfigurationType.PORTRAIT);
        final String oldPortraitKey = "portrait-pref";
        updateKeyboardPreference(oldPortraitKey, newPortraitKey);
    }

    private void updateKeyboardPreference(final String oldPreferenceKey,
            final String newPreferenceKey) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, PREFS_PERMS);
        SharedPreferences.Editor editor = settings.edit();

        if ( settings.contains(oldPreferenceKey) ) {
            if ( !settings.contains(newPreferenceKey) ) {
                String keyboardId = settings.getString(oldPreferenceKey, null);
                if ( keyboardId != null && mImePerConfig.get(newPreferenceKey) == null ) {
                    mImePerConfig.put(newPreferenceKey, keyboardId);
                    editor.putString(newPreferenceKey, keyboardId);
                }
            }
            editor.remove(oldPreferenceKey);
        }
        editor.commit();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, PREFS_PERMS);

        // Check for launchFlag
        boolean isActivated = settings.getBoolean(TOGGLE_STATE, false);
        if ( !isActivated ) {
            Log.w(TAG, "Keyboard Manager was disabled");
            this.stopSelf();
            return 0;
        }

        // Check if the application key is installed.
        mIsKeyEnabled = Utils.checkForKey(getApplicationContext()) == KMKeyState.UP_TO_DATE;

        String tickerText = null;
        if ( intent != null )
            tickerText = intent.getStringExtra(TICKER_INTENT_KEY);

        Bundle serviceBundle = null;
        if ( intent != null )
            serviceBundle = intent.getBundleExtra(SERVICE_BUNDLE_KEY);

        boolean iconFlag = settings.getBoolean(ICON_STATE, false);

        String currentAppPackageName = null;
        if ( serviceBundle != null ) {
            iconFlag = serviceBundle.getBoolean(ICON_STATE, iconFlag);

            // Check if the user allowed for screen transition
            if ( serviceBundle.containsKey(TRANSITION_SCREEN_STATE) )
                mTransitionAllowed = serviceBundle.getBoolean(TRANSITION_SCREEN_STATE);

            if ( serviceBundle.containsKey(Constants.KILL_PREV_IME) )
                mKillPrevIme = serviceBundle.getBoolean(Constants.KILL_PREV_IME);

            if ( serviceBundle.getBoolean(Constants.UPDATE_ENABLED_APP_LIST_KEY, false) )
                loadEnabledAppList(settings);

            // Set the keyboard id field variables
            for ( String prefKey : serviceBundle.keySet() ) {
                if ( prefKey.startsWith(Constants.KEYBOARD_PREF_PREFIX) ) {
                    String keyboardId = serviceBundle.getString(prefKey);
                    mImePerConfig.put(prefKey, keyboardId);
                }
            }

            // Retrieve the current app package name if it exists
            currentAppPackageName = serviceBundle.getString(Constants.APP_PACKAGE_NAME_KEY);
            if ( currentAppPackageName != null ) {
                Log.d(TAG, "Current package name: " + currentAppPackageName);
            }
        }

        if ( !configurationUpdate(getResources().getConfiguration(), false, currentAppPackageName) ) {
            stopSelf();
            return 0;
        }

        pushNotification(iconFlag, tickerText);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.releaseRoot();
        this.clearNotification();

        // Stop listening for docking events
        unregisterReceiver(mDockEventReceiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        configurationUpdate(newConfig, true, null);
    }

    protected boolean configurationUpdate(Configuration newConfig, boolean transitionFlag,
            final String currentPackageName) {
        return configurationUpdate(newConfig, transitionFlag, getDockState(), currentPackageName);
    }

    protected boolean configurationUpdate(Configuration newConfig, boolean transitionFlag,
            DockState dockState, final String currentPackageName) {

        ConfigurationType orientationConfig = null;
        ConfigurationType dockConfig = null;

        // Docking state has priority over other configuration
        switch (dockState) {
            case CAR:
                dockConfig = ConfigurationType.CAR_DOCK;
                break;

            case DESK:
                dockConfig = ConfigurationType.DESK_DOCK;
                break;

            case UNDOCKED:
            default:
                // nothing to do
                break;
        }

        if ( newConfig.keyboard != Configuration.KEYBOARD_NOKEYS
             && newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO ) {
            orientationConfig = ConfigurationType.HARDWARE;
        }
        else {
            int screenOrientation = newConfig.orientation;

            if ( screenOrientation == Configuration.ORIENTATION_LANDSCAPE ) {
                orientationConfig = ConfigurationType.LANDSCAPE;
            }
            else if ( screenOrientation == Configuration.ORIENTATION_PORTRAIT ) {
                orientationConfig = ConfigurationType.PORTRAIT;
            }
        }

        if ( orientationConfig == null && dockConfig == null )
            return false;

        String orientationKey = null;
        String dockKey = null;

        if ( mIsKeyEnabled ) {
            String currentAppTitle = getCurrentAppCmp(currentPackageName);

            if ( currentAppTitle != null ) {
                Log.d(TAG, "Current app is " + currentAppTitle);

                ComponentName currentApp = mEnabledApps.get(currentAppTitle);
                if ( currentApp != null ) {
                    Log.d(TAG,
                            "Current app component is "
                                    + currentApp.flattenToShortString());

                    if ( dockConfig != null ) {
                        dockKey = Utils.getAppKeyboardIdKey(currentApp, dockConfig);
                        if ( mImePerConfig.get(dockKey) != null )
                            return updateIme(dockKey, transitionFlag);
                    }

                    if ( orientationConfig != null ) {
                        orientationKey = Utils.getAppKeyboardIdKey(currentApp, orientationConfig);
                        if ( mImePerConfig.get(orientationKey) != null )
                            return updateIme(orientationKey, transitionFlag);
                    }
                }
            }
        }

        // Global preferences
        if ( dockConfig != null ) {
            dockKey = Utils.getGlobalKeyboardIdKey(dockConfig);
            if ( mImePerConfig.get(dockKey) != null )
                return updateIme(dockKey, transitionFlag);
        }

        if ( orientationConfig != null ) {
            orientationKey = Utils.getGlobalKeyboardIdKey(orientationConfig);
            if ( mImePerConfig.get(orientationKey) != null )
                return updateIme(orientationKey, transitionFlag);
        }

        return false;
    }

    protected void pushNotification(boolean iconFlag, String tickerText) {
        if ( tickerText == null || tickerText.equals("") )
            return;

        if ( iconFlag )
            this.statusNotification(tickerText);
        else {
            this.clearNotification();
            toastMessage(this, tickerText);
        }
    }

    protected void statusNotification(CharSequence tickerText) {
        // Get reference to the notification manager
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PackageManager pm = getPackageManager();
        if ( nManager == null || pm == null )
            return;

        Notification notification;
        CharSequence contentTitle = "Keyboard Manager";
        Context context = getApplicationContext();

        Intent notificationIntent = new Intent(this, KeyboardManager.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if ( this.mNotification == null ) {
            // Instantiate the notification
            notification = new Notification();
            notification.icon = R.drawable.statusbaricon;
            notification.tickerText = tickerText;
            notification.when = System.currentTimeMillis();
            notification.flags = Notification.FLAG_NO_CLEAR
                                 | Notification.FLAG_ONGOING_EVENT;
            notification.setLatestEventInfo(context, contentTitle, "", contentIntent);

            this.mNotification = notification;
        }
        else {
            // Updating the notification
            notification = this.mNotification;

            if ( tickerText.equals(notification.tickerText) )
                return;

            notification.tickerText = tickerText;
            notification.when = System.currentTimeMillis();
            notification.setLatestEventInfo(context, contentTitle, "", contentIntent);
        }

        nManager.notify(NOTIFICATION_ID, notification);
    }

    protected boolean updateIme(String configKey, boolean transitionFlag) {
        if ( configKey == null )
            return false;

        final String currId = this.mCurrId;
        String newKeyboardId = mImePerConfig.get(configKey);
        if ( newKeyboardId == null ) {
            Log.d(TAG, "No ime mapping for configuration " + configKey);
            return false;
        }

        try {
            if ( !newKeyboardId.equals(currId) ) {
                if ( transitionFlag && mTransitionAllowed ) {

                    /*
                     * Experimentation showed that switching the IMEs on the
                     * foreground activity through Keyboard Manager without some
                     * kind of transition (switching to another activity/dialog
                     * and back) would often cause the IME to appear in an
                     * invalid/unusable state.
                     */
                    Intent activityIntent = new Intent(getBaseContext(),
                            TransitionDialog.class);
                    activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activityIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activityIntent.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);

                    getApplication().startActivity(activityIntent);
                }

                final InputMethodManager imm = (InputMethodManager) this
                        .getSystemService(INPUT_METHOD_SERVICE);

                // stop the current ime
                if ( mKillPrevIme )
                    Utils.stopIme(this, currId);

                /*
                 * Switch the current IME to the new one. It's only possible to
                 * use setInputMethod with a null argument for the IBinder token
                 * if the caller is a system application
                 */
                imm.setInputMethod(null, newKeyboardId);

                // Send a notification broadcast for the change in IMEs
                Intent updateImeIntent = new Intent(Intent.ACTION_INPUT_METHOD_CHANGED);
                updateImeIntent.putExtra("input_method_id", newKeyboardId);
                sendBroadcast(updateImeIntent);

                this.mCurrId = newKeyboardId;
            }
        } catch (Exception e) {
            Log.e(TAG,
                    "Error occurred while switching keyboards to ".concat(newKeyboardId),
                    e);
            toastMessage(this, "Exception caught: ".concat(e.toString()));
            return false;
        }

        return true;
    }

    protected void clearNotification() {
        if ( this.mNotification == null )
            return;

        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if ( nManager == null )
            return;

        nManager.cancel(NOTIFICATION_ID);
        this.mNotification = null;
    }
}
