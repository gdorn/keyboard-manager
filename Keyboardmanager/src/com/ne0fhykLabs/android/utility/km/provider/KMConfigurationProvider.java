/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.ne0fhykLabs.android.utility.km.service.KeyboardSwitcher;
import com.ne0fhykLabs.android.utility.km.utils.Constants;

public class KMConfigurationProvider extends ContentProvider {

    private static final String TAG = KMConfigurationProvider.class.getName();

    /* (non-Javadoc)
     * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Don't support deletes
        return 0;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#getType(android.net.Uri)
     */
    @Override
    public String getType(Uri uri) {
        return Constants.CONTENT_MIME_TYPE;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if ( Constants.CONTENT_URI.equals(uri) ) {
            // Retrieve the value
            String packageName = values.getAsString(Constants.CURRENT_PACKAGE_NAME);
            Log.d(TAG, "Retrieved package name: " + packageName);

            // TODO: call start service with package name in intent extras
            Bundle serviceBundle = new Bundle(1);
            serviceBundle.putString(Constants.APP_PACKAGE_NAME_KEY, packageName);

            final Context context = getContext();
            context.startService(new Intent(context, KeyboardSwitcher.class)
                                          .putExtra(Constants.SERVICE_BUNDLE_KEY, serviceBundle));
        }

        // Since we don't support queries, there's no point returning an uri
        return null;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#onCreate()
     */
    @Override
    public boolean onCreate() {
        // Nothing to do
        return true;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        // Don't support query
        return null;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // Don't support updates
        return 0;
    }

}
