/*
 * Copyright (C) 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.utils;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.accessibilityservice.AccessibilityServiceInfoCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.util.Log;
import android.util.Pair;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.ne0fhykLabs.android.utility.km.activity.KeyboardManager;
import com.ne0fhykLabs.android.utility.km.service.KeyboardSwitcher;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.*;

/**
 * Stores utility methods for this application.
 *
 * @author Fredia Huya-Kouadio
 */
public class Utils {

    private static final String TAG = Utils.class.getName();

    public enum KillSignal {
        STOP,
        CONT,
        KILL,
        HUP,
        TERM
    }

    public enum Shell {
        sh,
        su
    }

    public enum ConfigurationType {
        PORTRAIT,
        LANDSCAPE,
        HARDWARE,
        BLUETOOTH,
        DESK_DOCK,
        CAR_DOCK
    }

    public enum KMKeyState {
        NOT_INSTALLED,
        NOT_UP_TO_DATE,
        UP_TO_DATE
    }

    // Root process
    public static Process rootProc;

    public static void toastMessage(Context caller, String message) {
        Toast.makeText(caller, message, Toast.LENGTH_LONG).show();
    }

    public static void logError(Context caller, String message) {
        Log.e(TAG, message);
        toastMessage(caller, message);
    }

    public static void logInfo(Context caller, String message) {
        Log.i(TAG, message);
        toastMessage(caller, message);
    }

    public static void logWarning(Context caller, String message) {
        Log.w(TAG, message);
        toastMessage(caller, message);
    }

    public static KMKeyState checkForKey(Context context) {
        try {
           PackageInfo pInfo = context.getPackageManager().getPackageInfo(Constants.KM_KEY_PACKAGE_NAME, 0);
            if ( pInfo.versionCode >= Constants.ACCESSIBILITY_SERVICE_LEAST_VERSION_CODE )
                return KMKeyState.UP_TO_DATE;

            return KMKeyState.NOT_UP_TO_DATE;
        } catch (NameNotFoundException e) {
            return KMKeyState.NOT_INSTALLED;
        }
    }

    public static boolean isAccessibilityServiceEnabled(final Context context) {
        AccessibilityManager am =
                (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        PackageManager pm = context.getPackageManager();

        List<AccessibilityServiceInfo> enabledServiceList = AccessibilityManagerCompat
                .getEnabledAccessibilityServiceList(am, AccessibilityServiceInfo.FEEDBACK_GENERIC);

        for ( AccessibilityServiceInfo info : enabledServiceList ) {
            ResolveInfo resolveInfo = AccessibilityServiceInfoCompat.getResolveInfo(info);
            String infoName = resolveInfo.loadLabel(pm).toString();
            if ( Constants.ACCESSIBILITY_SERVICE_LABEL.equals(infoName) ) {
                return true;
            }
        }

        return false;
    }

    public static String getGlobalKeyboardIdKey(ConfigurationType configuration) {
        return Constants.KEYBOARD_PREF_PREFIX + "." + Constants.GLOBAL_KEYBOARD_PREF_KEY + "."
               + configuration.name();
    }

    public static String getAppKeyboardIdKey(ComponentName appCmp,
            ConfigurationType configuration) {
        return Constants.KEYBOARD_PREF_PREFIX + "." + Constants.APP_KEYBOARD_PREF_KEY + "."
               + appCmp.flattenToShortString() + "." + configuration.name();
    }

    public static void disableKM(Context context) {
        SharedPreferences settings = context
                .getSharedPreferences(Constants.PREFS_NAME, Constants.PREFS_PERMS);

        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Constants.TOGGLE_STATE, false);
        editor.commit();
    }

    public static boolean acquireRoot(Context caller) {
        Log.i(TAG, "Requesting root priviledges");

        try {
            if ( rootProc != null )
                rootProc.destroy();

            rootProc = new ProcessBuilder("su").redirectErrorStream(true).start();
        } catch (Exception e) {
            logError(caller, "Unable to obtain root priviledges...");
            return false;
        }

        return true;
    }

    public static void releaseRoot() {
        Log.d(TAG, "Releasing root priviledges/process");

        if ( rootProc != null )
            rootProc.destroy();
    }

    public static HashMap<String, InputMethodInfo> getIdLabelMap(Context context) {
        InputMethodManager imeManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if ( imeManager == null )
            return null;

        List<InputMethodInfo> imesInfo = imeManager.getEnabledInputMethodList();
        HashMap<String, InputMethodInfo> idLabelMap = new HashMap<String, InputMethodInfo>(
                                                                                           imesInfo.size());

        for ( InputMethodInfo imi : imesInfo )
            idLabelMap.put(imi.getId(), imi);

        return idLabelMap;
    }

    public static void stopIme(Context caller, String imeId) {
        List<String> pidList = retrieveImePid(caller, imeId);
        if ( pidList == null ) {
            Log.e(TAG, "Error occurred while trying to retrieve pids for ime id: "
                    .concat(imeId));
            return;
        }

        int numPids = pidList.size();
        String[] killCommands = new String[numPids];
        for ( int i = 0; i < numPids; i++ )
            killCommands[i] = "kill -".concat(KillSignal.TERM.name()).concat(" ")
                    .concat(pidList.get(i));

        runCommand(Shell.su, killCommands);
    }

    public static List<String> retrieveImePid(Context caller, String imeId) {
        Map<String, InputMethodInfo> imeIdInfo = getIdLabelMap(caller);
        if ( !imeIdInfo.containsKey(imeId) )
            return null;

        InputMethodInfo currIme = imeIdInfo.get(imeId);
        if ( currIme == null )
            return null;

        // Get the current ime process name.
        String currImeProcessName = currIme.getServiceInfo().processName;
        return retrieveProcessPids(currImeProcessName);
    }

    public static int getPsPidIndex() {
        String command = "ps -h";
        String output = runCommand(Shell.sh, command);
        if ( output == null ) {
            Log.e(TAG, "Unable to run command ".concat(command));
            return Constants.DEFAULT_PS_PID_INDEX;
        }

        int numParts;
        String[] outputLines = output.split("\n");
        for ( String line : outputLines ) {
            String[] lineParts = line.split("\\s+");
            numParts = lineParts.length;

            for ( int i = 0; i < numParts; i++ ) {
                if ( lineParts[i].equals(Constants.PID_COL) )
                    return i;
            }
        }

        return Constants.DEFAULT_PS_PID_INDEX;
    }

    public static List<String> retrieveProcessPids(String processName) {
        String command = "ps|grep ".concat(processName);
        String output = runCommand(Shell.sh, command);
        if ( output == null ) {
            Log.e(TAG, "Unable to run command ".concat(command));
            return Collections.emptyList();
        }

        int pidColIndex = getPsPidIndex();

        String[] outputLines = output.split("\n");
        List<String> pidList = new ArrayList<String>(outputLines.length);
        for ( String line : outputLines ) {
            String[] lineParts = line.split("\\s+");
            if ( lineParts.length < 2 )
                continue;

            pidList.add(lineParts[pidColIndex]);
        }

        return pidList;
    }

    public static Pair<CharSequence[], CharSequence[]> getKeyboardsInfo(Context context) {
        // Retrieve list of input method
        InputMethodManager imeManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        List<InputMethodInfo> imes = imeManager.getEnabledInputMethodList();
        int imesSize = imes.size();

        // Store input method labels for display in view menu
        CharSequence[] keyboardLabels = new CharSequence[imesSize + 1];
        CharSequence[] keyboardIds = new CharSequence[imesSize + 1];

        keyboardLabels[0] = Constants.DEFAULT_KEYBOARD_LABEL;
        keyboardIds[0] = Constants.DEFAULT_KEYBOARD_ID;

        PackageManager pm = context.getPackageManager();
        for ( int i = 1; i < imesSize + 1; i++ ) {
            InputMethodInfo imi = imes.get(i - 1);
            keyboardLabels[i] = imi.loadLabel(pm);
            keyboardIds[i] = imi.getId();
        }

        return new Pair<CharSequence[], CharSequence[]>(keyboardLabels, keyboardIds);
    }

    public static String runCommand(Shell shell, String... commands) {
        try {
            Process shellProc = new ProcessBuilder(shell.name())
                    .redirectErrorStream(true).start();
            DataOutputStream shellIn = new DataOutputStream(shellProc.getOutputStream());
            DataInputStream shellOut = new DataInputStream(shellProc.getInputStream());

            int numCommands = commands.length;
            for ( int i = 0; i < numCommands; i++ ) {
                shellIn.writeBytes(commands[i].concat("\n"));
                shellIn.flush();
            }

            shellIn.close();
            shellProc.waitFor();

            int readOutBytes;
            int outBytes = shellOut.available();

            int bufferSize = 2096;
            byte[] outBuffer = new byte[bufferSize];

            StringBuilder outputBuilder = new StringBuilder();

            while (outBytes > 0) {
                readOutBytes = shellOut
                        .read(outBuffer, 0, Math.min(outBytes, bufferSize));
                if ( readOutBytes == -1 )
                    break;
                outputBuilder.append(new String(outBuffer, 0, readOutBytes));
                outBytes -= readOutBytes;
            }

            String output = outputBuilder.toString();

            shellOut.close();
            shellProc.destroy();

            return output;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            Log.e(TAG,
                  "Error occurred while running command "
                          .concat(Arrays.toString(commands)).concat(" with shell ")
                          .concat(shell.name()));
            return null;
        }
    }

    public static void installKMKeyDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Keyboard Manager Key Required!")
                .setMessage("Get the Keyboard Manager Key to enable this feature!")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Get Key!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse("market://details?id=" + Constants.KM_KEY_PACKAGE_NAME));
                        context.startActivity(marketIntent);
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static void updateKMKeyDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Update Keyboard Manager Key!")
                .setMessage("In order to enable this feature, Keyboard Manager Key needs to be " +
                            "version " + Constants.ACCESSIBILITY_SERVICE_LEAST_VERSION_NAME +
                                    " or higher.")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Update Key!", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse("market://details?id=" + Constants.KM_KEY_PACKAGE_NAME));
                        context.startActivity(marketIntent);
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static void enableAccessibilityServiceDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Enable KM Extras")
                .setMessage("In order to use the app specific keyboard,"
                                    +
                                    " you need to enable the Keyboard Manager Extras Accessibility Service.")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Enable KM Extras", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent settingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        activity.startActivity(settingsIntent);
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static void selectAppDialog(final Activity activity) {
        final Context context = activity.getApplicationContext();

        final Map<String, ComponentName> allAppsInfo = getAllApps(context);
        final Map<String, ComponentName> savedAppsInfo = getEnabledAppList(context);
        for ( String appTitle : savedAppsInfo.keySet() ) {
            if ( !allAppsInfo.containsKey(appTitle) )
                savedAppsInfo.remove(appTitle);
        }

        updateEnabledAppList(context, savedAppsInfo.values()
                .toArray(new ComponentName[0]));

        final String[] dialogItems = allAppsInfo.keySet().toArray(new String[0]);
        final boolean[] checkedDialogItems = new boolean[dialogItems.length];
        for ( int i = 0; i < dialogItems.length; i++ ) {
            if ( savedAppsInfo.containsKey(dialogItems[i]) )
                checkedDialogItems[i] = true;
            else
                checkedDialogItems[i] = false;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Select app(s)")
                .setMultiChoiceItems(dialogItems, checkedDialogItems,
                                     new DialogInterface.OnMultiChoiceClickListener() {

                                         @Override
                                         public void onClick(DialogInterface dialog, int which,
                                                 boolean isChecked) {
                                             String item = dialogItems[which];
                                             if ( isChecked ) {
                                                 ComponentName itemCmp = allAppsInfo.get(item);
                                                 savedAppsInfo.put(item, itemCmp);
                                             }
                                             else {
                                                 savedAppsInfo.remove(item);
                                             }
                                         }
                                     })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Done", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Store the list of checked apps to the shared
                        // preferences
                        ComponentName[] appsCmp = savedAppsInfo.values()
                                .toArray(
                                         new ComponentName[0]);

                        updateEnabledAppList(context, appsCmp);

                        // Tell the keyboard manager service to update its list
                        // of enabled apps.
                        Bundle serviceBundle = new Bundle(1);
                        serviceBundle.putBoolean(Constants.UPDATE_ENABLED_APP_LIST_KEY, true);
                        Intent serviceIntent = new Intent(context, KeyboardSwitcher.class)
                                .putExtra(Constants.SERVICE_BUNDLE_KEY, serviceBundle);
                        context.startService(serviceIntent);

                        if ( activity instanceof KeyboardManager )
                            ((KeyboardManager) activity).updateAppScreen();
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public static Map<String, ComponentName> getEnabledAppList(Context context) {
        SharedPreferences settings = context
                .getSharedPreferences(Constants.PREFS_NAME, Constants.PREFS_PERMS);

        String flattenedList = settings.getString(Constants.APPS_LIST_KEY, "");
        String[] appList = flattenedList.split("\\|");

        final Map<String, ComponentName> appsInfo = new TreeMap<String, ComponentName>();
        for ( String appCmp : appList ) {
            ComponentName cmp = restoreCmp(appCmp);
            if ( cmp == null )
                continue;
            String appTitle = getAppTitle(context, cmp);
            if ( appTitle == null )
                continue;

            appsInfo.put(appTitle, cmp);
        }

        // Update the saved preference with the valid entries.
        updateEnabledAppList(context, appsInfo.values().toArray(new ComponentName[0]));

        return appsInfo;
    }

    public static void updateEnabledAppList(Context context, ComponentName[] cmpList) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME,
                                                                       Constants.PREFS_PERMS)
                .edit();

        StringBuilder flattenedList = new StringBuilder();
        boolean first = true;
        for ( ComponentName cmp : cmpList ) {
            if ( first )
                first = false;
            else
                flattenedList.append("|");

            flattenedList.append(cmp.flattenToShortString());
        }

        editor.putString(Constants.APPS_LIST_KEY, flattenedList.toString()).commit();
    }

    public static void removeStaleAppPreferences(Context context, ComponentName[] staleCmpList) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_NAME,
                                                                       Constants.PREFS_PERMS)
                .edit();

        ConfigurationType[] configurations = ConfigurationType.values();

        for ( ComponentName staleCmp : staleCmpList ) {
            for ( ConfigurationType configuration : configurations ) {
                String prefKey = getAppKeyboardIdKey(staleCmp, configuration);
                editor.remove(prefKey);
            }
        }
        editor.commit();
    }

    public static ComponentName restoreCmp(String flattenedCmp) {
        if ( flattenedCmp.length() == 0 )
            return null;
        return ComponentName.unflattenFromString(flattenedCmp);
    }

    public static String getAppTitle(Context context, ComponentName cmp) {
        final PackageManager manager = context.getPackageManager();

        ActivityInfo info;
        try {
            info = manager.getActivityInfo(cmp, 0);
            return info.loadLabel(manager).toString();
        } catch (NameNotFoundException e) {
            Log.w(TAG, "Unable to find label for component name " + cmp);
            return null;
        }
    }

    public static Map<String, ComponentName> getAllApps(Context context) {
        final PackageManager manager = context.getPackageManager();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null)
                .addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> apps = manager.queryIntentActivities(mainIntent, 0);

        if ( apps == null ) {
            Log.e(TAG, "Unable to retrieve device applications.");
            return null;
        }

        Map<String, ComponentName> appsInfo = new TreeMap<String, ComponentName>();
        for ( ResolveInfo appInfo : apps ) {
            ComponentName appCmp = new ComponentName(appInfo.activityInfo.packageName,
                                                     appInfo.activityInfo.name);
            String appTitle = getAppTitle(context, appCmp);
            if ( appTitle == null )
                continue;

            appsInfo.put(appTitle, appCmp);
        }

        return appsInfo;
    }

}
