/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.utils;

import android.content.Context;
import android.net.Uri;

/**
 * Stores the constants for this application.
 *
 * @author ne0fhyk
 *
 */
public class Constants {

    public static final String INSTALLER_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.kmLauncher";
    public static final String KM_KEY_PACKAGE_NAME = "com.ne0fhykLabs.android.utility.extras.km.key";

    public static final String PREFS_NAME = "Keyboard-Manager-Preferences-1.3";
    public static final int PREFS_PERMS = Context.MODE_WORLD_READABLE
                                          | Context.MODE_WORLD_WRITEABLE;

    public static final String KEYBOARD_PREF_PREFIX = "keyboardId";
    public static final String GLOBAL_KEYBOARD_PREF_KEY = "global_pref";
    public static final String APP_KEYBOARD_PREF_KEY = "app_pref";
    public static final String APPS_KEYBOARD_PREF_SCREEN_KEY = "appskeyboard_pref";
    public static final String ADD_APPS_PREF_KEY = "add_apps_pref";
    public static final String APP_PACKAGE_NAME_KEY = "app_package_name_key";

    public static final String TOGGLE_STATE = "start-pref";
    public static final String TRANSITION_SCREEN_STATE = "transition-screen-pref";
    public static final String KILL_PREV_IME = "kill-prev-ime-pref";
    public static final String ICON_STATE = "notify-icon-pref";
    public static final String BOOT_STATE = "boot-pref";
    public static final String BACKUP_PREF = "backup_pref";
    public static final String RESTORE_PREF = "restore_pref";

    public static final String BACKUP_FILEPATH_PREF = "backup_filepath";
    public static final String DEFAULT_BACKUP_FILENAME = "km_preferences_backup.xml";

    public static final String UPDATE_ENABLED_APP_LIST_KEY = "update_enabled_app_list";

    public static final String APPS_LIST_KEY = "apps_list";

    public static final String ROOT_ACCESS = "root-access";

    public static final String SERVICE_BUNDLE_KEY = "com.ne0fhykLabs.android.utility.km.serviceBundle";
    public static final String TICKER_INTENT_KEY = "com.ne0fhykLabs.android.utility.km.tickerText";

    public static final String PID_COL = "PID";
    public static final int DEFAULT_PS_PID_INDEX = 1;

    public static final String DEFAULT_KEYBOARD_LABEL = "Default";
    public static final String DEFAULT_KEYBOARD_ID = "default_id";

    public static final String ACCESSIBILITY_SERVICE_LABEL = "Keyboard Manager Extras";
    public static final int ACCESSIBILITY_SERVICE_LEAST_VERSION_CODE = 15;
    public static final String ACCESSIBILITY_SERVICE_LEAST_VERSION_NAME = "2.6";

    /*
     * Provider constants
     */
    public static final String PROVIDER_AUTHORITY = "com.ne0fhykLabs.android.utility.km.KMConfigurationProvider";
    public static final Uri PROVIDER_URI = Uri.parse("content://" + PROVIDER_AUTHORITY);
    public static final Uri CONTENT_URI = Uri.withAppendedPath(PROVIDER_URI, "current_app");
    public static final String CONTENT_MIME_TYPE = "text/plain";
    public static final String CURRENT_PACKAGE_NAME = "current_package_name";

    /**
     * Private constructor to prevent class instantiation.
     * @since 2.11
     */
    private Constants(){}

}
