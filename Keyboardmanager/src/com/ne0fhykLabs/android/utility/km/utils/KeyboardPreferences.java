/*
 * Copyright (C) 2012, 2013 Fredia Huya-Kouadio <fhuyakou@gmail.com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.ne0fhykLabs.android.utility.km.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

import com.ne0fhykLabs.android.utility.km.R;
import com.ne0fhykLabs.android.utility.km.activity.KeyboardManager.KeyboardPreferenceChangeListener;
import com.ne0fhykLabs.android.utility.km.utils.Utils.ConfigurationType;
import com.ne0fhykLabs.android.utility.km.utils.Utils.KMKeyState;

public class KeyboardPreferences extends DialogPreference {

    public KeyboardPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.keyboard_preferences);
        setPositiveButtonText("Done");
        setNegativeButtonText(null);

        setPersistent(false);
    }

    @Override
    public void onBindDialogView(View view) {
        View portraitPref = view.findViewById(R.id.portraitPref);
        portraitPref.setOnClickListener(new KeyboardOptionListener(
                ConfigurationType.PORTRAIT));

        View landscapePref = view.findViewById(R.id.landscapePref);
        landscapePref.setOnClickListener(new KeyboardOptionListener(
                ConfigurationType.LANDSCAPE));

        View hardwarePref = view.findViewById(R.id.hardwarePref);
        hardwarePref.setOnClickListener(new KeyboardOptionListener(
                ConfigurationType.HARDWARE));

        // Extra preferences
        // TODO: check if possible to detect bluetooth keyboard, then update appropriately
        View bluetoothPref = view.findViewById(R.id.bluetoothPref);
        bluetoothPref.setVisibility(View.GONE);
        bluetoothPref.setOnClickListener(new KeyboardOptionListener(ConfigurationType.BLUETOOTH,
                                                                    true));

        View dockDeskPref = view.findViewById(R.id.dock_desk_pref);
        dockDeskPref.setOnClickListener(new KeyboardOptionListener(ConfigurationType.DESK_DOCK,
                                                                   true));

        View dockCarPref = view.findViewById(R.id.dock_car_pref);
        dockCarPref
                .setOnClickListener(new KeyboardOptionListener(ConfigurationType.CAR_DOCK, true));

        super.onBindDialogView(view);
    }

    private void launchKeyboardList(final ConfigurationType configuration) {
        final Context context = getContext();
        final OnPreferenceChangeListener prefListener = getOnPreferenceChangeListener();

        Pair<CharSequence[], CharSequence[]> keyboardsInfo = Utils.getKeyboardsInfo(context);

        final CharSequence[] keyboardLabels = keyboardsInfo.first;
        final CharSequence[] keyboardIds = keyboardsInfo.second;

        final SharedPreferences preferences = getSharedPreferences();

        final String configurationName = configuration.name();
        final String keyboardPreferenceKey = Constants.KEYBOARD_PREF_PREFIX + "." + getKey()
                + "." + configurationName;
        String defaultKeyboardId = preferences.getString(keyboardPreferenceKey,
                keyboardIds[0].toString());

        int checkedItemIndex = 0;
        for (int i = 0; i < keyboardIds.length; i++) {
            if (keyboardIds[i].equals(defaultKeyboardId)) {
                checkedItemIndex = i;
                break;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose input method ( " + configurationName + " )")
                .setSingleChoiceItems(keyboardLabels, checkedItemIndex,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final String keyboardId = keyboardIds[which].toString();
                                SharedPreferences.Editor editor = preferences.edit();
                                String updatedKeyboardId = null;

                                if (!keyboardId.equals(Constants.DEFAULT_KEYBOARD_ID)) {
                                    updatedKeyboardId = keyboardId;

                                    // Store the keyboard id
                                    editor.putString(keyboardPreferenceKey, keyboardId);
                                } else {
                                    // Remove this key's preference.
                                    editor.remove(keyboardPreferenceKey);
                                }

                                // Notify the preference change listener
                                if (prefListener instanceof KeyboardPreferenceChangeListener) {
                                    ((KeyboardPreferenceChangeListener) prefListener)
                                            .onPreferenceChange(keyboardPreferenceKey,
                                                    updatedKeyboardId);
                                }

                                editor.commit();
                                dialog.dismiss();
                            }
                        }).create().show();
    }

    private class KeyboardOptionListener implements View.OnClickListener {

        private final ConfigurationType mConfiguration;
        private final boolean mCheckForKey;

        private KeyboardOptionListener(final ConfigurationType configuration) {
            mConfiguration = configuration;
            mCheckForKey = false;
        }

        private KeyboardOptionListener(final ConfigurationType configuration,
                final boolean checkForKey) {
            mConfiguration = configuration;
            mCheckForKey = checkForKey;
        }

        @Override
        public void onClick(View view) {
            if ( mCheckForKey ) {
                final Context context = getContext();
                KMKeyState keyState = Utils.checkForKey(context);
                if ( keyState == KMKeyState.NOT_INSTALLED ) {
                    Utils.installKMKeyDialog(context);
                    return;
                }
                else if ( keyState == KMKeyState.NOT_UP_TO_DATE ) {
                    Utils.updateKMKeyDialog(context);
                    return;
                }
            }

            launchKeyboardList(mConfiguration);
        }
    }

}
